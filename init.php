<?php

system('php composer install');
if (!is_dir('www/assets')) {
	mkdir('www/assets', 0777);
}
if (!is_dir('www/protected/runtime')) {
	mkdir('www/protected/runtime', 0777);
}
if (!is_file('www/protected/config/config-constants.php')) {
	copy('www/protected/config/config-constants.example.php', 'www/protected/config/config-constants.php');
}
