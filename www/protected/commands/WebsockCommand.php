<?php
require '../../vendor/autoload.php';
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Websock\Chat;

class WebsockCommand extends CConsoleCommand
{
	public function actionIndex()
	{
		$server = IoServer::factory(
			new HttpServer(
				new WsServer(
					new Chat()
				)
			),
			8080
		);

		$server->run();
	}
}