<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<?php

		\Yii::app()->bootstrap;
		
	?>	

                <script type="text/javascript">
		<?php if ( @isset ( $this->tourCount ) ): ?>
                    var tourCount = <?php echo $this->tourCount; ?>;
		<?php endif ?>
		<?php if ( @isset ( $this->questionInTourCount ) ): ?>
                    var questionInTourCount = <?php echo $this->questionInTourCount; ?>;
		<?php endif ?>
                </script>
		<?php if (isset($this->scripts)): ?>
			<?php foreach ($this->scripts as $script): ?>
				<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl.'/js/'.$script ?>"></script>
			<?php endforeach; ?>
		<?php endif ?>

		<?php if (isset($this->styles)): ?>
			<?php foreach ($this->styles as $style): ?>
				<link href="<?php echo Yii::app()->request->baseUrl.'/css/'.$style ?>" media="screen" rel="stylesheet" type="text/css" />
			<?php endforeach; ?>
		<?php endif ?>				
		
	</head>
	<body>
		<div class="container well">
			<!--Branch: PhotoAnswers-->
			<div class="pull-right"><a href="<?php echo $this->createAbsoluteUrl('site/info') ?>">Информация</a></div>
<?php			
	$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
		'homeLink' => CHtml::link('Главная', Yii::app()->request->baseUrl.'/'),
		'links'=>isset($this->breadcrumbs) ? $this->breadcrumbs : array(),
	));	
?>
			
			<?php echo $content ?>
		</div>
	</body>
</html>
