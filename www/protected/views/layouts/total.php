<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript">
            	    var tourCount = <?php echo $this->tourCount; ?>;
            	    var questionInTourCount = <?php echo $this->questionInTourCount; ?>;
                </script>
		<!--script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl.'/js/results.js' ?>"></script-->
		<link href="<?php echo Yii::app()->request->baseUrl.'/css/results.css' ?>" media="screen" rel="stylesheet" type="text/css" />
		<?php  
			Yii::app()->clientScript->registerCoreScript('jquery')
		?>
	</head>
	<body class="results-back">
		<div class="results-header">
			<div class="results-logo"><?php echo "Играется ;; вопрос"?></div>
			<div class="results-tour-num" id="results-tour-num"></div>
		</div>
		
		<table class="results-table" cellspacing="0">
			<thead>
			<tr>
				<td rowspan="2"><img src="/images/1_place.png"></td>
				<td rowspan="2"><img src="/images/2_name.png"></td>
				<td colspan="<?php echo $this->questionInTourCount?>" class="results-table-answers"><img src="/images/3_tour_result.png"></td>
				<td rowspan="2"><img src="/images/4_round_scores.png"></td>
				<td rowspan="2"><img src="/images/5_overall_result.png"></td>
			</tr>
			<tr>
				<?php for ($i=1; $i <= $this->questionInTourCount; $i++ ): ?>
					<td class="results-table-one-answer head"><?php echo $i; ?></td>
				<?php endfor; ?>
			</tr>
			</thead>
			
			<tbody>
				<?php for($i=0; $i<100; $i++): ?>
					<tr id="row<?php echo $i?>">
					<td><span class="results-place"></span></td>
					<td class="results-team-cell"><span class="results-team"></span></td>
					<?php for ( $j = 0; $j < $this->questionInTourCount; $j ++ ):?>
					<td class="results-table-one-answer results-answer"><span></span></td>
					<?php endfor; ?>

					<td><span class="results-tour-points"></span></td>
					<td><span class="results-points"></span></td>						
					</tr>
				<?php endfor; ?>
		
				
			</tbody>
		
		</table>
		
	</body>
</html>
