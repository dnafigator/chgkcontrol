<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<?php

		\Yii::app()->bootstrap;
		
	?>	

		<?php if (isset($this->scripts)): ?>
			<?php foreach ($this->scripts as $script): ?>
				<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl.'/js/'.$script ?>"></script>
			<?php endforeach; ?>
		<?php endif ?>

		<?php if (isset($this->styles)): ?>
			<?php foreach ($this->styles as $style): ?>
				<link href="<?php echo Yii::app()->request->baseUrl.'/css/'.$style ?>" media="screen" rel="stylesheet" type="text/css" />
			<?php endforeach; ?>
		<?php endif ?>				
		
	</head>
	<body>
		<div class="well">
			<div class="pull-right"><a href="site/info">����������</a></div>

			<?php echo $content ?>
		</div>
	</body>
</html>
