<h4>Регистрация команд</h4>


<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'TeamListOps',
	'type' => 'horizontal',
	'enableClientValidation' => false,
	'action' => $this->createAbsoluteUrl('registration/upload'),
	'htmlOptions'=>array(
		'method' => 'POST',
		'style' => 'padding-top:10px',
		'enctype' => 'multipart/form-data'
	),
));
?>

<div>
	<?php
	echo $form->fileField($model, 'teamListFile');
	$this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'action', 'label'=>'Загрузить'));
	$this->endWidget();
	?>
</div>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'createTeam',
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'action' => $this->createAbsoluteUrl('registration/create'),
	'htmlOptions'=>array(
		'method' => 'POST',
		'style' => 'padding-top:10px',
	),
));
?>


<?php echo $form->textFieldRow($model, 'num', array(
	'class'=>'span3',
	'placeholder' => 'Номер'
));
?>

<?php echo $form->textFieldRow($model, 'rating_id', array(
	'class'=>'span3',
	'placeholder' => 'ID команды'
));
?>

<?php echo $form->textFieldRow($model, 'name', array(
	'class'=>'span6',
	'placeholder' => 'Название команды'
));
?>

<?php echo $form->textFieldRow($model, 'city', array(
	'class'=>'span6',
	'placeholder' => 'Город'
));
?>
Подсветить
<?php echo $form->checkBox($model, 'highlight',
	array(
		'class'=>'span6',
		'placeholder' => 'Подсветить'
	));

?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Зарегистрировать')); ?>
</div>

<?php $this->endWidget(); ?>

<div class="pull-right">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'type'=>'action', 'url' => $this->createAbsoluteUrl('registration/download'), 'label'=>'Скачать')); ?>
</div><br/>

<?php

if (! isset($gridModel)) {
	$gridModel = $model;
}

$this->widget(
	'bootstrap.widgets.TbExtendedGridView', array(
	'dataProvider' => $gridModel->search(),
	'template' => "{items} {pager}",
	'type' => 'striped bordered',
	'filter' => $gridModel,
	'htmlOptions' => array(
		'style' => 'padding-top:15px;'
	),
	'columns' => array(
		array(
			'name' => 'num',
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'editable' => array(
				'type' => 'text',
				'url' => $this->createAbsoluteUrl('registration/update'),
				'title' => 'Введите номер команды'
			),
			'htmlOptions' => array(
				'style' => 'width:100px'
			)
		),
		array(
			'name' => 'rating_id',
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'editable' => array(
				'type' => 'text',
				'url' => $this->createAbsoluteUrl('registration/update'),
				'title' => 'Введите ID команды'
			),
			'htmlOptions' => array(
				'style' => 'width:100px'
			)
		),
		array(
			'name' => 'name',
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'editable' => array(
				'type' => 'text',
				'url' => $this->createAbsoluteUrl('registration/update'),
				'title' => 'Введите название команды'
			)
		),
		array(
			'name' => 'city',
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'editable' => array(
				'type' => 'text',
				'url' => $this->createAbsoluteUrl('registration/update'),
				'title' => 'Введите название города'
			)
		),
		array(
			'name' => 'highlight',
			'class' => 'bootstrap.widgets.TbEditableColumn',
			'editable' => array(
				'type' => 'text',
				'url' => $this->createAbsoluteUrl('registration/update'),
				'title' => 'Введите признак подсветки'
			)
		),
		array(
			'name' => 'points',
			'filter' => false
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'header' => 'Удалить',
			'template' => '{delete}',
			'htmlOptions' => array(
				'style'=>'text-align:center',
			),
		)
	),
));
?>
<div class="pull-right">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'type'=>'primary', 'url' => $this->createAbsoluteUrl('runtime/index'), 'label'=>'Играть')); ?>
</div>
<script lang="js">
	$('#TeamListOps').on('submit', function(){
		return confirm('Загрузка списка удалит все команды и результаты. Продолжить?');
	})
</script>