<a href="javascript:history.back()">Назад</a><br/>
Разработка: <a target="_blank" href="http://prosvdigital.com">Prosveshchenie Digital</a>, 2014<br/>
Последние изменения <?php echo $LastChanges?>

<h3>Главная страница</h3>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image15.png"/><br/>
<span>Всё очевидно. Пункты меню могут добавляться, удаляться, меняться местами.</span>
<h3>Новая игра</h3>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image04.png"/><br/>
<span>Всё очевидно. При нажатии на кнопку выводится предупреждение, после чего происходит сброс всех данных и переход на страницу регистрации.</span>


<h3>Регистрация команд</h3>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image10.png"/><br/>
<span>Можно вводить команды вручную, при этом и ID и название должны быть уникальны. Можно загрузить список команд из CSV-файла.<br/>
Пример файла:<br/>
"Номер","ID команды","Название","Город"<br/>
1,416,Потанинцы,Воронеж<br/>
2,4622,Спонсора.net,Воронеж<br/>
Шаблон файла можно получить, нажав на кнопку «Скачать» и изменить его в любом табличном или текстовом редакторе редакторе: LibreOffice, OpenOffice, Microsoft Office, Google Spreadsheet, notepad и т.п.<br/>
При нажатии на кнопку «Играть» вы попадаете на страницу игры.
</span>


<h3>Управление игрой</h3>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image07.png"/><br/>
<span>Список команд отсортирован по алфавиту</span><br/>

<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image09.png"/><br/>
<span>Меняет номер тура, отображаемого на странице «Общий список с прокруткой» (доступна с главной).</span><br/>

<h3></h3>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image18.png"/><br/>
<span>Номер отображаемого на странице управления тура.</span><br/>

<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image19.png"/><br/>
<span>Номер вопроса, с которым в данный момент происходит работа.</span><br/>

<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image12.png"/><br/>
<span>Сохраняет все не сохранённые ранее результаты «Текущего вопроса» в «Текущем туре». При этом правильные ответы из красных плюсов превращаются в чёрные. Номер вопроса автоматически увеличивается.</span><br/>

<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image20.png"/><br/>
<span>Добавляет правильный ответ одной из команд.
Возможные варианты:
<table style="border:1" border="1">
	<tr>
		<td>000012300456</td>
		<td>Штрихкод в формате 0000TTT00QQC<br/>
T — номер команды<br/>
Q — номер вопроса<br/>
C — контрольная сумма
		</td>
	</tr>
	<tr>
		<td>13 25</td>
		<td>Ответ на вопрос №13 команды №25</td>
	</tr>
	<tr>
		<td>10</td>
		<td>Ответ на «Текущий вопрос» «Текущего тура» команды №10. Рекомендуется всегда использовать этот вариант при вводе ответов вручную</td>
	</tr>
	<tr>
		<td>2987000000006</td>
		<td>Ответ на «Текущий вопрос» «Текущего тура» команды №987. Рекомендуется всегда использовать этот вариант со сканером штрихкодов</td>
	</tr>
</table>
После ввода ответов, они отмечаются как несохранённые (красным плюсом). При этом они не отображаются на общем табло и не сохраняются при скачивании результатов.<br/>
<b>Всегда проверяйте, все ли ответы сохранены!</b><br/><br/><br/>
</span>

<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image00.png"/><br/>
<span>При нажатии на кнопку «Сохранить», плюсы «чернеют» и текущий вопрос меняется на следующий. Если это был последний вопрос тура, то тур не меняется и номер текущего вопроса не увеличивается.</span><br/>

<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image13.png"/><br/>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image11.png"/><br/>
<span>Для отмены ответа можно ввести код в любом из вышеперечисленных форматах в это поле. Будет выдано предупреждение, какой ответ какой команды вы отменяете.
    Ничего сохранять не нужно т.е. «красного минуса» вы не увидите. Обратите внимание, что при отмене ответа отменяется как правильный, так и спорный ответы.<br/><b>С осторожностью пользуйтесь этой функцией!</b>
</span><br/>

<h3>Спорные ответы</h3>
<span>Спорные ответы вводятся при нажатии на кнопку <img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image21.png"/><br/>

При этом открывается форма ввода спорного ответа.<br/>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image22.png"/><br/>

Код вводится в любом формате (в т.ч. сканером), в этом поле можно жать Enter, форма не закроется и фокус переместиться в поле для текста ответа. Текст вводить не обязательно, если он будет пустой, то в выгрузке результатов будет &laquo;?&raquo;<br/>
Если ответ уже был спорным, то текст его заменится.<br/>
Приоритет у правильного ответа! Т.е. если ответ был введён и как спорный, и как правильный, то он считается правильным! (При отображении может работать некорректно, т.е. вводишь спорный поверх правильного — отображается как спорный, сохраняешь или обновляешь страницу — отображается как правильный)<br/>
Чтобы сделать спорный ответ правильным, достаточно засчитать его как правильный.<br/>
Чтобы сделать правильный спорным, нужно ОТМЕНИТЬ его и ввести его как спорный.<br/>
Ответ в таблице при этом отмечается &laquo;?&raquo; со ссылкой.<br/>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image23.png"/><br/>
Нажав на этот &laquo;?&raquo;, можно отредактировать текст ответа.<br/>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image24.png"/><br/>
Код при редактировании менять нельзя.
</span><br/>

<h3>Архив игр</h3>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image25.png"/><br/>
В разделе &laquo;Архив игр&raquo; содержится список всех прошедших игр. Дата и время — это дата и время нажатия кнопки «Начать новую игру».<br/>
В каждой игре (пока) лежат (только) файлы результатов. Они автоматически сохраняются при каждом скачивании и перед началом новой игры.<br/>
<img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image26.png"/>

<h3>Идеальный ход игры</h3>
<span>Если «Всё Идёт Так» (В противовес «Что-то пошло не так»), то игра выглядит следующим образом.</span>
<ol>
	<li><img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image05.png"/></li>
	<li><img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image03.png"/></li>
	<li><img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image02.png"/></li>
	<li><img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image14.png"/></li>
	<li><img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image17.png"/></li>
	<li><img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image01.png"/><br/>
		<span>Для каждого вопроса быстро вбиваете номера команд, давшие на него правильный ответ. Или сканируете штрихкоды с бланков</span>
	</li>
	<li><img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image12.png"/><br/>
	<span>При сохранении вопрос автоматически меняется на следующий.</span></li>
        <li>При наличии спорных, вводим их. <img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image21.png"/></li>
	<li>Шаги 6, 7 и 8 повторить 3 подхода по 12 раз (или сколько там туров и вопросов в текущей игре).</li>
	<li>Выйти на главную и нажать <img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image08.png"/></li>
	<li>Посмотреть на <img src="<?php echo Yii::app()->getBaseUrl(true)?>/images/help/image06.png"/> (на главной странице). Объявить его в торжественной обстановке и, на всякий случай, сохранить (Ctrl+S)</li>
</ol>
<span>Кажется, всё.</span><br/>
<span>По вопросам пишите: <span>dan@bronyakin.ru</span>


