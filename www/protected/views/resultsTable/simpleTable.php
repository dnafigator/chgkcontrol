<?php  
	Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish('css/resultsSimpleTable.css'
	));	
?>

<h3>Таблица результатов</h3>

<table class="table-bordered table" cellspacing="0">
	<thead>
	<tr>
		<td style="text-align: center" rowspan="2"><a href="<?php echo $this->createAbsoluteUrl( $this->uniqueid.'/'.$this->action->Id )?>/points">Место</a></td>
		<td style="text-align: center" rowspan="2"><a href="<?php echo $this->createAbsoluteUrl( $this->uniqueid.'/'.$this->action->Id )?>/id">Номер</a></td>
		<td style="text-align: center" rowspan="2"><a href="<?php echo $this->createAbsoluteUrl( $this->uniqueid.'/'.$this->action->Id )?>/name">Название</a></td>
		<td style="text-align: center" rowspan="2">Баллы</td>
		<td style="text-align: center" colspan="<?php echo $questionInTourCount*$tourCount?>" class="results-table-answers">Результаты игры</td>
	</tr>
	<tr>
		<?php for($i=1; $i<=$questionInTourCount*$tourCount; $i++): ?>
			<td style="text-align: center"><?php echo $i?></td>
		<?php endfor; ?>
	</tr>
	</thead>

	<tbody>
		<?php foreach($teamsData as $team): ?>
			<tr id="row<?php echo $i?>">
			<td><?php echo $team['place'] ?></td>
			<td><?php echo $team['id'] ?></td>			
			<td class="results-team-cell"><?php echo $team['name'].' ('.$team['city'].')' ?></td>
			<td id="points<?php echo $team['id'] ?>" style="text-align: center"><?php echo $team['points'] ?></td>						

			<?php for ($i=1; $i<=$questionInTourCount*$tourCount; $i++): ?>
				
			<?php
				if (! isset($team['tour_answers']["$i"])) {
					$noCommited = false;
				}
				else {
					$noCommited = $team['tour_answers']["$i"]['commited'] == 0;
				}
				if (! isset($team['tour_arguable_answers']["$i"])) {
					$noCommited2 = false;
				}
				else {
					$noCommited2 = $team['tour_arguable_answers']["$i"]['commited'] == 0;
				}
			?>
			
				<td style="text-align: center" id="<?php echo 'answer'.$team['id'].'_'.$i ?>" class="<?php echo ($noCommited|$noCommited2) ? 'no-commited' : '' ?> <?php echo 'team'.$team['id'].' question'.$i?>">
					<?php 
						if ( isset($team['tour_answers']["$i"]) )
						{
							echo '+';
						}
						else if ( isset($team['tour_arguable_answers']["$i"]) )
						{
							echo '?';
						}
						else
						{
							echo '-';
						}
					?>
				</td>
			<?php endfor; ?>

			</tr>
		<?php endforeach; ?>


	</tbody>

</table>

