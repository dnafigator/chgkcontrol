<?php  
	Yii::app()->clientScript->registerScriptFile(
			Yii::app()->assetManager->publish('js/photoAnswer.js'
	));
	
	Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish('css/photoAnswer.css'
	));	

function findTeamByID ( $id, $teams )
{
	foreach ( $teams as $team )
	{
		if ( $id == $team [ 'id' ] )
		{
			return $team;
		}
	}
	return null;
}

?>
<script>
	var currentQuestion = <?php echo $currentQuestion;?>;
</script>
<h3>Фотоответы</h3>

<!--<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'type'=>'primary', 'id' => 'complete', 'label'=>'Сохранить и закрыть')); ?>&nbsp;-->
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'type'=>'action', 'id' => 'close', 'label'=>'Готово')); ?><br/>

<?php 
foreach ( $images as $img )
{
		$team = findTeamByID ( $img['team'], $answers );
		
		if ( $team ):
			$right = isset ( $team [ 'tour_answers' ] [ $img['question'] ] );
			?><div class="thumbnail photoanswer text-center<?php if ($right) echo ' right-answer';?>">
				<?php echo $team['id'].' '.$team['name'].'('.$team['city'].')';?>&nbsp;Вопрос&nbsp;<?php echo $img['question'];?>
				<img src="<?php echo '/upload/'.$currentFolder.'/'.$img['image'];?>">
				<?php if ( $right ):?>
				<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'id' => $img['question'].' '.$team['id'], 'htmlOptions'=>array('class'=>'wrongAnswer'), 'type'=>'action', 'label'=>'Отменить')); ?>
				<?php else:?>
				<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'link', 'id' => $img['question'].' '.$team['id'], 'htmlOptions'=>array('class'=>'rightAnswer'), 'type'=>'action','label'=>'Засчитать')); ?>
				<?php endif;?>
				
			</div>
		<?php endif;
	}
?>


