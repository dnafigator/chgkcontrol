<?php
Yii::app()->clientScript->registerScriptFile(
	Yii::app()->assetManager->publish('js/runtime.js'
	));

Yii::app()->clientScript->registerCssFile(
	Yii::app()->assetManager->publish('css/runtime.css'
	));

//print_r ( $teamsData );
?>

<h3>Управление игрой</h3>

<form id="resultsForm" class="form-inline pull-right">
	<label for="showTour" class="control-label">Отображать на табло тур №: </label>&nbsp;&nbsp;
	<select id="showTour" name="showTour" class="span2">
		<?php for($i=1; $i<= $tourCount; $i++): ?>
			<option <?php echo $showTour == $i ? 'selected' : '' ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php endfor; ?>
	</select>
</form>
<br/><br/>

<!--h5>Текущий тур/вопрос</h5-->
<form id="commitForm" class="form-inline" method="GET">
	<label class="control-label" for="tourNum">Текущий тур</label>&nbsp;&nbsp;
	<select id="tourNum" name="commitTour" class="span1">
		<?php for ($i=1; $i<=$tourCount; $i++): ?>

			<option <?php echo $commitTour == $i ? 'selected' : '' ?> value="<?php echo $i?>"><?php echo $i?></option>
		<?php endfor; ?>
	</select>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<label class="control-label" for="questionNum">Текущий вопрос</label>&nbsp;&nbsp;
	<select id="questionNum" name="commitQuestion" class="span1">
		<?php for ($i=1; $i<=$questionInTourCount; $i++): ?>
			<option <?php echo $commitQuestion == $i ? 'selected' : '' ?> value="<?php echo $i?>"><?php echo $i + ($commitTour-1)*$questionInTourCount?></option>
		<?php endfor; ?>
	</select>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="button" id="commitAnswers" value="Сохранить" class="btn-danger"/>
</form>

<div class="pull-left">
	<!--h5>Сохранение ответов</h5-->
	<form id="answersForm" class="form-inline">
		<!--<label for="addAnswerCode" class="control-label">Код для добавления</label>&nbsp;&nbsp;-->
		<input type="text" id="addAnswerCode" autocomplete="off"/>
		<input type="submit" value="Добавить ответ" class="btn-warning"/>
		<input type="button" id="aAnswersInit" value="Спорный ответ" class="btn-action"/>
		<!--input id="showphotos" type="button" value="Фото" class="btn-ok"/-->
	</form>
</div>

<div id="aAnswerFormDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="aAnswerFormLabel" aria-hidden="true">
	<form id="aAnswerForm" class="form-inline">
		<div class="modal-header">
			<h3 id="aAnswerFormLabel">Ввод спорного ответа</h3>
		</div>
		<div class="modal-body">
			<label for="aAnswerCode" class="control-label">Код для добавления</label>&nbsp;&nbsp;<input type="text" id="aAnswerCode" autocomplete="off"/><br/>
			<label for="aAnswerText" class="control-label">Текст ответа команды</label>&nbsp;&nbsp;<input type="text" name="aAnswerText" id="aAnswerText" autocomplete="off" />
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true" id="aAnswerCancel">Закрыть</button>
			<button class="btn btn-primary" data-dismiss="modal" id="aAnswerSubmit">OK</button>
		</div>
	</form>
</div>


<div class="pull-right">
	<!--h5>Отмена ответов</h5-->
	<form id="answersCancelForm" class="form-inline">
		<!--<label for="deleteAnswerCode" class="control-label">Код для отмены</label>&nbsp;&nbsp;-->
		<input type="text" id="deleteAnswerCode" autocomplete="off" />
		<input type="submit" value="Отменить ответ" class="btn-danger"/>
	</form>
</div><br/>

<!--h5>Результаты</h5-->
<table class="table-bordered table" cellspacing="0">
	<thead>
	<tr><?php
		//sort
		function cmpId($a, $b) {
			return $a['id'] - $b['id'];
		}

		function cmpIdR($a, $b) {
			return cmpId($b, $a);
		}

		function cmpPlace($a, $b) {
			return $a['place'] - $b['place'];
		}

		function cmpPlaceR($a, $b) {
			return cmpPlace($b, $a);
		}

		function cmpName($a, $b) {
			return strcmp(mb_strtolower($a['name']), mb_strtolower($b['name']));
		}

		function cmpNameR($a, $b) {
			return cmpName($b, $a);
		}

		function cmpTourPoints($a, $b) {
			return $a['tour_points'] - $b['tour_points'];
		}

		function cmpTourPointsR($a, $b) {
			return cmpTourPoints($b, $a);
		}

		function cmpPoints($a, $b) {
			return $a['points'] - $b['points'];
		}

		function cmpPointsR($a, $b) {
			return cmpPoints($b, $a);
		}

		$sortLink = [];
		for($i = 0; $i <= 5 + $questionInTourCount; $i++ ) {
			$val = 2*$i;
			$sym = '';
			if ( $val == $sort ) {
				$sym = '▲';
				$val++;
			} else if ( $val + 1 == $sort ) {
				$sym = '▼';
			}
			$sortLink[$i] = '<a href="?sort='.$val.'">'.$sym;
		}

		switch($sort) {
			case '0':
				usort($teamsData, 'cmpPlace');
				break;
			case '1':
				usort($teamsData, 'cmpPlaceR');
				break;
			case '2':
				usort($teamsData, 'cmpId');
				break;
			case '3':
				usort($teamsData, 'cmpIdR');
				break;
			case '4':
				usort($teamsData, 'cmpName');
				break;
			case '5':
				usort($teamsData, 'cmpNameR');
				break;
			case '6':
				usort($teamsData, 'cmpTourPoints');
				break;
			case '7':
				usort($teamsData, 'cmpTourPointsR');
				break;
			case '8':
				usort($teamsData, 'cmpPoints');
				break;
			case '9':
				usort($teamsData, 'cmpPointsR');
				break;
			default:
				$cmpAnswersQ = floor(($sort-10)/2);
				if ( $sort % 2 ){
					usort($teamsData, function($a,$b) use($cmpAnswersQ) {
						$pa = (isset($a['tour_answers'][$cmpAnswersQ]) ? 4 : 0) +
							($a['tour_answers'][$cmpAnswersQ]['commited'] ? 1 : 0) +
							(isset($a['tour_arguable_answers'][$cmpAnswersQ]) ? 2 : 0) +
							($a['tour_arguable_answers'][$cmpAnswersQ]['commited'] ? 1 : 0);
						$pb = (isset($b['tour_answers'][$cmpAnswersQ]) ? 4 : 0) +
							($b['tour_answers'][$cmpAnswersQ]['commited'] ? 1 : 0) +
							(isset($b['tour_arguable_answers'][$cmpAnswersQ]) ? 2 : 0) +
							($b['tour_arguable_answers'][$cmpAnswersQ]['commited'] ? 1 : 0);
						return $pa - $pb;
					});
				} else {
					usort($teamsData,  function($a,$b) use($cmpAnswersQ) {
						$pa = (isset($a['tour_answers'][$cmpAnswersQ]) ? 4 : 0) +
							($a['tour_answers'][$cmpAnswersQ]['commited'] ? 1 : 0) +
							(isset($a['tour_arguable_answers'][$cmpAnswersQ]) ? 2 : 0) +
							($a['tour_arguable_answers'][$cmpAnswersQ]['commit'] ? 1 : 0);
						$pb = (isset($b['tour_answers'][$cmpAnswersQ]) ? 4 : 0) +
							($b['tour_answers'][$cmpAnswersQ]['commited'] ? 1 : 0) +
							(isset($b['tour_arguable_answers'][$cmpAnswersQ]) ? 2 : 0) +
							($b['tour_arguable_answers'][$cmpAnswersQ]['commited'] ? 1 : 0);
						return $pb - $pa;
					});
				}
				break;
		}
		?>
		<td style="text-align: center" rowspan="2"><?=$sortLink[0]?>Место</a></td>
		<td style="text-align: center" rowspan="2"><?=$sortLink[1]?>ID</a></td>
		<td style="text-align: center" rowspan="2"><?=$sortLink[2]?>Название</a></td>
		<td style="text-align: center" colspan="<?php echo $questionInTourCount?>" class="results-table-answers">Результаты тура</td>
		<td style="text-align: center" rowspan="2"><?=$sortLink[3]?>Баллы за тур</a></td>
		<td style="text-align: center" rowspan="2"><?=$sortLink[4]?>Текущий результат</a></td>
	</tr>
	<tr>
		<?php for($i=1; $i<=$questionInTourCount; $i++): ?>
			<td style="text-align: center"><?=$sortLink[5+$i]?><?php echo $i + ($commitTour-1)*$questionInTourCount ?></a></td>
		<?php endfor; ?>
	</tr>
	</thead>

	<tbody>
	<?php
	$questionData = [];
	foreach($teamsData as $team): ?>
		<tr id="row<?php echo $i?>">
			<td><?php echo $team['place'] ?></td>
			<td><?php echo $team['num'] ?></td>
			<td class="results-team-cell"><?php echo $team['name'].' ('.$team['city'].')' ?></td>

			<?php 
			for ($i=1; $i<=$questionInTourCount; $i++):
				$questionData[$i] = 0;
				$noCommited = $noCommited2 = false;
				if (isset($team['tour_answers']["$i"])) {
					$noCommited = $team['tour_answers']["$i"]['commited'] == 0;
					$questionData[$i] ++;
				}
				if ( isset($team['tour_arguable_answers']["$i"])) {
					$noCommited2 = $team['tour_arguable_answers']["$i"]['commited'] == 0;
				}
				?>

				<td style="text-align: center" id="<?php echo 'answer'.$team['num'].'_'.$i ?>" class="<?php echo ($noCommited|$noCommited2) ? 'no-commited' : '' ?> <?php echo 'team'.$team['num'].' question'.$i?>">
					<?php
					if ( isset($team['tour_answers']["$i"]) )
					{
						echo '+';
					}
					else if ( isset($team['tour_arguable_answers']["$i"]) )
					{
						$linkid='aAnswerEdit_'.($i + ($commitTour-1)*$questionInTourCount).'_'.$team['num'];

						?><a href="#" id="<?php echo $linkid;?>" >?</a><script>
						$('#<?php echo $linkid;?>').on('click', function(){
							$('#aAnswerText').val('<?php echo $team['tour_arguable_answers']["$i"]['text']; ?>');
							$('#aAnswerCode').val('<?php echo ($i + ($commitTour-1)*$questionInTourCount).' '.$team['num'] ?>');
							$("#aAnswerCode").attr('disabled','disabled');
							$('#aAnswerFormDialog').one('shown.bs.modal', 0, function (){
								$('#aAnswerText').focus();
							}).modal('show');
							return false;
						});
					</script><?php }
					else
					{
						echo '-';
					}
					?>
				</td>
			<?php endfor; ?>

			<td id="tp<?php echo $team['num'] ?>" style="text-align: center"><?php echo $team['tour_points'] ?></td>
			<td id="points<?php echo $team['num'] ?>" style="text-align: center"><?php echo $team['points'] ?></td>
		</tr>
	<?php endforeach; ?>
		<tr class="answered">
			<td colspan="3">Команды, взявшие вопрос</td>
			<?php for ($i=1; $i<=$questionInTourCount; $i++): ?>
				<td  class="answeredQ<?=$i?>"><?=$questionData[$i]?></td>
			<?endfor;?>
			<td colspan="2"></td>
		</tr><?$cnt = sizeof($teamsData);?>
		<tr data-count="<?=$cnt?>" class="answeredPercent">
			<td colspan="3">Процент взявших команд</td>
			<?php for ($i=1; $i<=$questionInTourCount; $i++): ?>
				<td class="answeredPercentQ<?=$i?>"><?=((int)(10000*$questionData[$i]/$cnt))/100?></td>
			<?endfor;?>
			<td colspan="2"></td>
		</tr>
	</tbody>

</table>

