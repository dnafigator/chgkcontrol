Текущая игра: <?=$current_name?><br/><br/>
<form id="deleteAllForm" action="<?php echo $this->createAbsoluteUrl('newGame/newGame') ?>">
    Название игры: <input type="text" name="game_name" value=""/><br/>
    Количество туров: <input type="text" name="tour_count" value="<?php echo $tourCount?>"/><br/>
    Количество вопросов в туре: <input type="text" name="question_in_tour_count"  value="<?php echo $questionInTourCount?>"/><br/>
    <input type="submit" value="Начать новую игру" id="deleteAll" class="btn-danger"/>
</form>
<hr>
Или
<form id="selectGameForm" action="<?php echo $this->createAbsoluteUrl('newGame/selectGame') ?>">
    <select name="game_id">
        <?foreach ( $games as $game):?>
            <option value="<?=$game->id?>"<?=$current_id == $game->id?' selected':''?>><?=$game->name?></option>
        <?endforeach;?>
    </select><br/>
    <input type="submit" value="Выбрать игру" id="select" class="btn-warning"/>
</form>

<script type="text/javascript">
    $('#deleteAllForm').on('submit', function(){
        return confirm('Вы действительно хотите начать новую игру?');
    });
</script>