<?php

class TeamTest extends CTestCase
{
	public function testCalcPlaces1()
	{
		$teams = array();

		$team = new Team();
		$team->points = 6;
		$teams[] = $team;
		
		$team = new Team();
		$team->points = 4;
		$teams[] = $team;
		
		$team = new Team();
		$team->points = 1;
		$teams[] = $team;
		
		Team::model()->calcPlaces($teams);
		$this->assertEquals(1, $teams[0]->place);
		$this->assertEquals(2, $teams[1]->place);
		$this->assertEquals(3, $teams[2]->place);
	}
	
	public function testCalcPlaces2()
	{
		$teams = array();

		$team = new Team();
		$team->points = 6;
		$teams[] = $team;
		
		$team = new Team();
		$team->points = 4;
		$teams[] = $team;
		
		$team = new Team();
		$team->points = 4;
		$teams[] = $team;		
		
		Team::model()->calcPlaces($teams);
		$this->assertEquals(1, $teams[0]->place);
		$this->assertEquals('2 - 3', $teams[1]->place);
		$this->assertEquals('2 - 3', $teams[2]->place);		
	}
	
	public function testCalcPlaces3()
	{
		$teams = array();

		$team = new Team();
		$team->points = 6;
		$teams[] = $team;
		
		$team = new Team();
		$team->points = 4;
		$teams[] = $team;
		
		$team = new Team();
		$team->points = 4;
		$teams[] = $team;		
		
		$team = new Team();
		$team->points = 3;
		$teams[] = $team;				
		
		$team = new Team();
		$team->points = 2;
		$teams[] = $team;
		
		$team = new Team();
		$team->points = 2;
		$teams[] = $team;
		
		$team = new Team();
		$team->points = 2;
		$teams[] = $team;		
		
		$team = new Team();
		$team->points = 1;
		$teams[] = $team;				
		
		$team = new Team();
		$team->points = 1;
		$teams[] = $team;						
		
		Team::model()->calcPlaces($teams);
		$this->assertEquals('1', $teams[0]->place);
		$this->assertEquals('2 - 3', $teams[1]->place);
		$this->assertEquals('2 - 3', $teams[2]->place);		
		$this->assertEquals('4', $teams[3]->place);		
		$this->assertEquals('5 - 7', $teams[4]->place);		
		$this->assertEquals('5 - 7', $teams[5]->place);		
		$this->assertEquals('5 - 7', $teams[6]->place);		
		$this->assertEquals('8 - 9', $teams[7]->place);		
		$this->assertEquals('8 - 9', $teams[8]->place);		
	}	
}