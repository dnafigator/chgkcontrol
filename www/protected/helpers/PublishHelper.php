<?php

class PublishHelper {

    public static function publish()
    {
        $runtimeData = RuntimeData::model()->getData ();
        if(!strlen($runtimeData->publish_url)) {
            return;
        }

        $data = Team::model()->getAllWithResults();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $runtimeData->publish_url.'/results/publish',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'publish_key' => $runtimeData->publish_key,
                'tour_count' => $runtimeData->tour_count,
                'question_in_tour_count' => $runtimeData->question_in_tour_count,
                'data' => json_encode($data),
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        //echo $response;
    }

    public static function get($data)
    {
        $res = PublishedResults::model()->findByAttributes(array('key' => $data['publish_key']));
        $ret = 'changed';
        if (!$res) {
            $res = new PublishedResults();
            $res->name = 'name';
            $res->key = $data['publish_key'];
            $res->tour_count = $data['tour_count'];
            $res->q_in_tour_count = $data['question_in_tour_count'];
            $ret = 'created';
        }
        $res->data = $data['data'];
        if (!$res->save()) {
            $ret = 'error';
        }
        return json_encode(array('status' => $ret));
    }
}