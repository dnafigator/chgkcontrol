<?php

class m140913_072313_create_table_chgk_team_arguableanswer extends CDbMigration
{
	public function up()
	{
		$this->createTable('chgk_team_arguableanswer', array(
			'id' => 'pk',
			'team_id' => 'int(11) not null default 0',
			'question_id' => 'int(11) not null default 0',
			'text' => 'varchar(300)'
		), 'ENGINE=InnoDB CHARSET=utf8');		
	}

	public function down()
	{
		$this->dropTable('chgk_team_arguableanswer');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}