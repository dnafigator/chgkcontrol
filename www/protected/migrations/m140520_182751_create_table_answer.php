<?php

class m140520_182751_create_table_answer extends CDbMigration
{
	public function up()
	{
		$this->createTable('chgk_team_answer', array(
			'id' => 'pk',
			'team_id' => 'int(11) not null default 0',
			'question_id' => 'int(11) not null default 0',
			'tour_num' => 'int(11) not null default 0',
			'question_num' => 'int(11) not null default 0',
			'commited' => 'tinyint(1) not null default 0'
		), 'ENGINE=InnoDB CHARSET=utf8');		
	}

	public function down()
	{
		$this->dropTable('chgk_team_answer');
	}
}