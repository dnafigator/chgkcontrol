<?php

class m160522_083638_create_table_published_results extends CDbMigration
{
	public function up()
	{
		$this->createTable('chgk_published_results', array(
			'id' => 'pk',
			'name' => 'varchar(300) NOT NULL',
			'dt' => 'datetime',
			'tour_count' => 'int(11)',
			'q_in_tour_count' => 'int(11)',
			'key' => 'varchar(255)',
			'data' => 'text',
		), 'ENGINE=InnoDB CHARSET=utf8');

		$this->addColumn ( 'chgk_runtime_data', 'publish_game', 'int(11)' );
		$this->addColumn ( 'chgk_runtime_data', 'publish_url', 'varchar(255)' );
		$this->addColumn ( 'chgk_runtime_data', 'publish_key', 'varchar(255)' );

	}

	public function down()
	{
		$this->dropTable('chgk_published_results');
		$this->dropColumn ( 'chgk_runtime_data', 'publish_game');
		$this->dropColumn ( 'chgk_runtime_data', 'publish_url');
		$this->dropColumn ( 'chgk_runtime_data', 'publish_key');
	}
}