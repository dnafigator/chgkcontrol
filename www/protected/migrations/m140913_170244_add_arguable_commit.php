<?php

class m140913_170244_add_arguable_commit extends CDbMigration
{
	public function up()
	{
		$this->addColumn('chgk_team_arguableanswer', 'commited', 'tinyint(1) not null default 0');
	}

	public function down()
	{
		$this->dropColumn('chgk_team_arguableanswer', 'commited');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}