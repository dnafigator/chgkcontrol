<?php

class m140521_140718_add_column_city_in_team extends CDbMigration
{
	public function up()
	{
		$this->addColumn('chgk_team', 'city', 'varchar(100) not null');
	}

	public function down()
	{
		$this->dropColumn('chgk_team', 'city');
	}
}