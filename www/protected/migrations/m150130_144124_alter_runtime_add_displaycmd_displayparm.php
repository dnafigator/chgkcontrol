<?php

class m150130_144124_alter_runtime_add_displaycmd_displayparm extends CDbMigration
{
	public function up()
	{
	    $this->addColumn ( 'chgk_runtime_data', 'displaycmd', 'varchar(255)' );
	    $this->addColumn ( 'chgk_runtime_data', 'displayparm', 'varchar(255)' );
	}

	public function down()
	{
	    $this->dropColumn ( 'chgk_runtime_data', 'displaycmd' );
	    $this->dropColumn ( 'chgk_runtime_data', 'displayparm' );
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}