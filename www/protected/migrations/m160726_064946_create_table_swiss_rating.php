<?php

class m160726_064946_create_table_swiss_rating extends CDbMigration
{
	public function up()
	{
		$this->createTable('chgk_swiss_rating', array(
			'id' => 'int(11) not null PRIMARY KEY',
			'rating' => 'int(11) not null default 0'
		), 'ENGINE=InnoDB CHARSET=utf8');
	}

	public function down()
	{
		$this->dropTable('chgk_swiss_rating');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}