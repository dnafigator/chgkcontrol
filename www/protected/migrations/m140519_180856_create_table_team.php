<?php

class m140519_180856_create_table_team extends CDbMigration {

	public function up() {
		$this->createTable('chgk_team', array(
			'id' => 'int(11) not null PRIMARY KEY',
			'name' => 'varchar(300) NOT NULL',
			'points' => 'int(11) not null default 0'
		), 'ENGINE=InnoDB CHARSET=utf8');
		
		$this->createIndex('points', 'chgk_team', 'points');
	}

	public function down() {
		$this->dropTable('chgk_team');
	}

}