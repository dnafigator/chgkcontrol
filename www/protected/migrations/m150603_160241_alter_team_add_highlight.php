<?php

class m150603_160241_alter_team_add_highlight extends CDbMigration
{
	public function up()
	{
		$this->addColumn('chgk_team', 'highlight', 'int(11) default 0');
	}

	public function down()
	{
		$this->dropColumn('chgk_team', 'highlight');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}