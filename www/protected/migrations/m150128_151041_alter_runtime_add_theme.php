<?php

class m150128_151041_alter_runtime_add_theme extends CDbMigration
{
	public function up()
	{
	    $this->addColumn ( 'chgk_runtime_data', 'theme', 'varchar(255)' );
	}

	public function down()
	{
	    $this->dropColumn ( 'chgk_runtime_data', 'theme' );
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}