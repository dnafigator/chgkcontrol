<?php

class m160514_090140_add_current_game extends CDbMigration
{
	public function up()
	{
		$this->createTable('chgk_game', array(
			'id' => 'pk',
			'name' => 'varchar(300) NOT NULL',
			'tour_count' => 'int(11) default 3',
			'question_in_tour_count' => 'int(11) default 12',
			'current_folder' => 'varchar(300) NOT NULL',
			'start' => 'timestamp',
			'end' => 'timestamp',
		), 'ENGINE=InnoDB CHARSET=utf8');

		$this->addColumn ( 'chgk_runtime_data', 'current_game', 'int(11) after id' );
		$this->addColumn ( 'chgk_runtime_data', 'game_name', 'varchar(300) NOT NULL AFTER current_game' );

		$this->addColumn ( 'chgk_team', 'num', 'int(11) after id' );
		$this->addColumn ( 'chgk_team', 'game_id', 'int(11) after id' );

		$this->alterColumn('chgk_team', 'id', 'int(11) not null');
		$this->dropPrimaryKey('id', 'chgk_team');
		$this->alterColumn('chgk_team', 'id', 'pk');

	}

	public function down()
	{
		$this->dropColumn ( 'chgk_runtime_data', 'current_game' );
		$this->dropColumn ( 'chgk_runtime_data', 'game_name' );
		$this->dropColumn ( 'chgk_team', 'num');
		$this->dropColumn ( 'chgk_team', 'game_id');
		$this->alterColumn('chgk_team', 'id', 'int(11) not null');
		$this->dropPrimaryKey('id', 'chgk_team');
		$this->alterColumn('chgk_team', 'id', 'int(11) not null PRIMARY KEY');
		$this->dropTable('chgk_game');
	}
}