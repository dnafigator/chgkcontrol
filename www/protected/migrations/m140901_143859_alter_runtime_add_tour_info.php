<?php

class m140901_143859_alter_runtime_add_tour_info extends CDbMigration
{
	public function up()
	{
	    $this->addColumn ( 'chgk_runtime_data', 'tour_count', 'int(11) default 3' );
	    $this->addColumn ( 'chgk_runtime_data', 'question_in_tour_count', 'int(11) default 12' );
	}

	public function down()
	{
   		$this->dropColumn ( 'chgk_runtime_data', 'tour_count' );
		$this->dropColumn ( 'chgk_runtime_data', 'question_in_tour_count' );
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}