<?php

class m140911_090854_alter_team_add_rating_id extends CDbMigration
{
	public function up()
	{
		$this->addColumn('chgk_team', 'rating_id', 'int(11)');
	}

	public function down()
	{
		$this->dropColumn('chgk_team', 'rating_id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}