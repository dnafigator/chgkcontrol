<?php

class m140602_162055_drop_column_question_num extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('chgk_team_answer', 'question_num');
	}

	public function down()
	{
		$this->addColumn('chgk_team_answer', 'question_num', 'int(11) not null');
	}
}