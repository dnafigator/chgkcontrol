<?php

class m140602_190913_add_teams_list extends CDbMigration
{
	public function up()
	{
		$this->truncateTable('chgk_team');
		$this->truncateTable('chgk_team_answer');
		return;
		$lines = file(dirname(__FILE__).'/teams.txt');
		foreach ($lines as $line) {
			$line = trim($line);
			if (! $line) {
				continue;
			}
			$data = explode('|', $line);
			$this->insert('chgk_team', array(
				'id' => trim($data[0]),
				'name' => trim($data[1]),
				'city' => trim($data[2]),
			));
		}
	}

	public function down()
	{
		$this->truncateTable('chgk_team');
		$this->truncateTable('chgk_team_answer');		
	}
}