<?php

class m140906_204912_alter_runtime_add_folder extends CDbMigration
{
	public function up()
	{
	    $this->addColumn ( 'chgk_runtime_data', 'current_folder', 'varchar(300) NOT NULL' );
	}

	public function down()
	{
   		$this->dropColumn ( 'chgk_runtime_data', 'current_folder' );
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}