<?php

class m140521_132203_create_table_chgk_runtime_data extends CDbMigration
{
	public function up()
	{
		$this->createTable('chgk_runtime_data', array(
			'id' => 'pk',
			'current_tour' => 'int(11) not null default 0',
			'current_question' => 'int(11) not null default 0',
			'last_update' => 'int(11) not null default 0'
		), 'ENGINE=InnoDB CHARSET=utf8');			
/*		
		$this->insert('chgk_runtime_data', array(
			'id' => 1,
			'current_tour' => 1,
			'current_question' => 1,
			'last_update' => 0
		));
*/
	}

	public function down()
	{
		$this->dropTable('chgk_runtime_data');
	}
}