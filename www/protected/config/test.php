<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			'db'=>array(
				'connectionString' => 'mysql:host='.DB_HOST.';dbname='.DB_NAME.'_test',
				'emulatePrepare' => true,
				'username' => DB_USER,
				'password' => DB_PASSW,
				'charset' => 'utf8',
			),
		),
	)
);
