<?php

class RuntimeData extends CActiveRecord
{
	private static $_data = null;
	
	public static function model($className = __CLASS__) 
	{
		return parent::model($className);
	}
	
	public function tableName() 
	{
		return 'chgk_runtime_data';
	}
	
	public function getData()
	{
		if (self::$_data === null) {
			self::$_data = self::model()->findByPk(1);
			if (self::$_data === null) {
				$m = new RuntimeData();
				$m->setAttributes(array(
					'id' => 1,
				));
				$m->save();
			}
		}
		return self::$_data;
	}
	
	public function setData($data)
	{
		$model = $this->getData();
		$model->setAttributes($data);
		$model->save();
	}
	
	public function rules()
	{
		return array(
			array('current_game', 'safe'),
			array('game_name', 'safe'),
			array('current_tour', 'safe'),
			array('current_question', 'safe'),
			array('last_update', 'safe'),
			array('tour_count', 'safe'),
			array('question_in_tour_count', 'safe'),
			array('current_folder', 'safe'),
			array('publish_game', 'safe'),
			array('publish_url', 'safe'),
			array('publish_key', 'safe'),
		);
	}
}