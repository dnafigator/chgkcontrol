<?php

function cmpByName ( $i1, $i2 )
{
    if ( $i1 [ 'name' ] > $i2 [ 'name' ] )
    {
        return 1;
    }
    if ( $i1 [ 'name' ] < $i2 [ 'name' ] )
    {
        return -1;
    }
    return 0;
}
function cmpByID ( $i1, $i2 )
{
    if ( $i1 [ 'id' ] > $i2 [ 'id' ] )
    {
        return 1;
    }
    if ( $i1 [ 'id' ] < $i2 [ 'id' ] )
    {
        return -1;
    }
    return 0;
}


/**
 * This is the model class for table "chgk_team".
 *
 * The followings are the available columns in table 'chgk_team':
 * @property integer $id
 * @property integer $game_id
 * @property integer $num
 * @property string $name
 * @property integer $points
 * @property integer $rating_id
 * @property integer $highlight
 */
class Team extends CActiveRecord
{
    public $place;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'chgk_team';
    }

    /**
     * @return array validation rules for model attributes.
     */

    public $teamListFile;
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('game_id, num, name', 'required'),
            array('id', 'unique'),
            array('city', 'safe'),
            array('rating_id', 'safe'),
            array('highlight', 'numerical'),
            array('id, game_id, num, points', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>300),
            array('teamListFile', 'file', 'types'=>'csv', 'on' => 'fileUpload'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, game_id, num, name, points, city, rating_id, highlight', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'game_id' => 'Игра',
            'num' => 'Номер',
            'name' => 'Название',
            'points' => 'Очки',
            'city' => 'Город',
            'rating_id' => 'ID команды',
            'highlight' => 'Подсветка',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->join = 'JOIN chgk_runtime_data ON chgk_runtime_data.current_game=game_id';
        $criteria->compare('num',$this->num);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('city',$this->city,true);
        $criteria->compare('rating_id',$this->rating_id,true);
        $criteria->compare('highlight',$this->highlight,true);
        //$criteria->compare('points',$this->points);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 100
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Team the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getTourPoints($tourNum)
    {
        return TeamAnswer::model()->countByAttributes(array(
            'team_id' => $this->id,
            'tour_num' => $tourNum,
            'commited' => 1
        ));
    }
    public function getAnswersMap($tourNum=null, $onlyCommited=true)
    {

        $runtimeData = RuntimeData::model()->getData ();
        $questionInTourCount = $runtimeData->question_in_tour_count;

        $attr = array(
            'team_id' => $this->id,
            //'tour_num' => $tourNum,
        );
        if ($tourNum !== null) {
            $attr['tour_num'] = $tourNum;
        }
        if ($onlyCommited) {
            $attr['commited'] = 1;
        }

        $answers = TeamAnswer::model()->findAllByAttributes($attr, array('select' => array('id', 'question_id', 'commited')));

        $result = array();
        foreach ($answers as $answ) {

            if ($tourNum !== null) {
                $qNum = $answ->question_id % $questionInTourCount;
                if ($qNum === 0) {
                    $qNum = $questionInTourCount;
                }
            }
            else {
                $qNum = $answ->question_id;
            }

            $result[$qNum] = array(
                'commited' => $answ['commited']
            );
        }

        return $result;
    }



    public function getArguableAnswersMap($tourNum=null,$onlyCommited=true)
    {

        $runtimeData = RuntimeData::model()->getData ();
        $questionInTourCount = $runtimeData->question_in_tour_count;

        $attr = array(
            'team_id' => $this->id,
        );
        if ($onlyCommited) {
            $attr['commited'] = 1;
        }

        $answers = TeamArguableAnswer::model()->findAllByAttributes($attr, array('select' => array('id', 'question_id', 'commited', 'text')));

        $result = array();
        foreach ($answers as $answ) {

            $ok = true;
            if ($tourNum !== null) {
                if ( ( $answ->question_id <= ($tourNum-1) * $questionInTourCount ) || ( $answ->question_id > ($tourNum) * $questionInTourCount ) )
                {
                    $ok = false;
                }
                else
                {
                    $qNum = $answ->question_id % $questionInTourCount;
                    if ($qNum === 0) {
                        $qNum = $questionInTourCount;
                    }
                }
            }
            else {
                $qNum = $answ->question_id;
            }


            if ( $ok )
            {
                $result[$qNum] = array(
                    'text' => $answ['text'],
                    'commited' => $answ['commited']
                );
            }
        }
        return $result;
    }

    const
        ORDER_POINTS = 1,
        ORDER_ID = 2,
        ORDER_NAME = 3;

    public function getAllWithResults($tourNum=null, $onlyCommited=true, $orderBy = Team::ORDER_POINTS )
    {

        $criteria = new CDbCriteria;
        $criteria->join = 'JOIN chgk_runtime_data ON chgk_runtime_data.current_game=game_id';
        $criteria->order = 'points desc';
        $teams = Team::model()->findAll($criteria);

        if (!$teams) {
            return array();
        }
        $this->calcPlaces($teams);
        $data = array();

        foreach ($teams as $team) {

            $tourAnswers = $team->getAnswersMap($tourNum, $onlyCommited);
            $tourAAnswers = $team->getArguableAnswersMap($tourNum,$onlyCommited);

            $tourPoints = 0;
            if ($onlyCommited) {
                $tourPoints = count($tourAnswers);
            }
            else {
                foreach ($tourAnswers as $ta) {
                    if ($ta['commited']) {
                        $tourPoints++;
                    }
                }
            }

            $data[] = array(
                'id' => $team->id,
                'game_id' => $team->game_id,
                'num' => $team->num,
                'name' => $team->name,
                'city' => $team->city,
                'rating_id' => $team->rating_id,
                'highlight' => $team->highlight,
                'points' => $team->points,
                'tour_answers' => $tourAnswers,
                'tour_arguable_answers' => $tourAAnswers,
                'tour_points' => $tourPoints,
                'place' => $team->place
            );
        }

        switch ( $orderBy )
        {
            case Team::ORDER_ID:
            {
                usort ( $data, 'cmpByID' );
                break;
            }
            case Team::ORDER_NAME:
            {
                usort ( $data, 'cmpByName' );
                break;
            }
        }
        return $data;
    }

    //TODO: сделать!
    public function getTeamResults ( $onlyCommited=true )
    {

        $attr = array(
            'id' => $this->id,
        );
        if ($onlyCommited) {
            $attr['commited'] = 1;
        }

        $answers = TeamAnswer::model()->findAllByAttributes($attr, array('select' => array('id', 'question_id', 'commited')));

        $result = array();
        foreach ($answers as $answ) {
            $qNum = $answ->question_id;
            $result[$qNum] = array(
                'commited' => $answ['commited']
            );
        }

        return $result;

    }

    public function calcPlaces(&$teams)
    {
        $total = count($teams);
        if (! $total) {
            return;
        }

        $clust = array();
        foreach ($teams as $team) {
            if (! isset($clust[$team->points])) {
                $clust[$team->points] = 1;
            }
            else {
                ++$clust[$team->points];
            }
        }

        $prevPoints = $teams[0]->points;
        $place = 1;
        for ($i=0; $i<$total; $i++) {

            $team = $teams[$i];

            if ($team->points < $prevPoints) {
                $place += $clust[$prevPoints];
                $prevPoints = $team->points;
            }
            $place2 = $clust[$team->points] + $place - 1;

            if ($place == $place2) {
                $team->place = $place;
            }
            else {
                $team->place = "$place - $place2";
            }
        }
    }


    public function truncateTable()
    {
        $this->getDbConnection()->createCommand()->truncateTable($this->tableName());
    }
}
