<?php

/**
 * This is the model class for table "chgk_team_arguableanswer".
 *
 * The followings are the available columns in table 'chgk_team_arguableanswer':
 * @property integer $id
 * @property integer $team_id
 * @property integer $question_id
 * @property string $text
 * @property integer $commited
 */
class TeamArguableAnswer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chgk_team_arguableanswer';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TeamArguableAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function addArguableAnswer($teamId, $questionId, $text)
	{
		$ta = self::model()->find('team_id=:teamId AND question_id=:questionId', array('teamId' => $teamId, 'questionId' => $questionId));
		if ( !$ta )
		{
			$ta = new TeamArguableAnswer();
			$ta->team_id = $teamId;
			$ta->question_id = $questionId;
		}
		$ta->commited = 0;
		$ta->text = $text;
		$ta->save();			
	}
	
	public function commitAnswers($questionId)
	{
		$tr = Yii::app()->db->beginTransaction();

		$tas = self::model()->findAllByAttributes(array(
			'commited' => 0,
			'question_id' => $questionId
		));

		foreach ($tas as $ta) {
			$ta->commited=1;
			$ta->save();
		}
		$tr->commit();		
	}
}
