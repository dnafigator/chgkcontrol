<?php

class PhotoAnswer extends CActiveRecord
{

	var $nameFormat = '/([0-9]+)_([0-9]+)_([0-9]+)\.jpg/';

	var $currentQuestion = 0;

	public function getPhotos ( $current_folder, $question = 0 )
	{
		$folderName = Yii::app()->basePath.'/../upload/'.$current_folder;
		$folder = opendir ( $folderName );
		$imgs = array ();
		$this->currentQuestion = $question;
		if ( $folder )
		{
			while ( $f = readdir ( $folder ) )
			{
				if ( is_file ( $folderName.'/'.$f ) )
				{
					if ( preg_match ( $this->nameFormat, $f, $m ) )
					{
						$img = array ( 'team' => $m [ 2 ], 'question' => $m [ 1 ], 'time' => $m [ 3 ], 'image' => $f );
						if ( 0 == $question || $img['question'] == $question )
						{
							$imgs[] = $img;
						}
					}
				}
			}
		}
		return $imgs;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function attributeNames ()
	{
		return array ( 'currentQuestion' );
	}
}
