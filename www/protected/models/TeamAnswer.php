<?php

/**
 * This is the model class for table "chgk_team_answer".
 *
 * The followings are the available columns in table 'chgk_team_answer':
 * @property integer $id
 * @property integer $team_id
 * @property integer $question_id
 * @property integer $tour_num
 * @property integer $commited
 */
class TeamAnswer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chgk_team_answer';
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TeamAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function addAnswer($teamNum, $questionId, $tourNum)
	{
		$criteria = new CDbCriteria();
		$criteria->join = 'JOIN chgk_runtime_data ON chgk_runtime_data.current_game=game_id';
		$criteria->addColumnCondition(array('num'=>$teamNum));
		$team = Team::model()->find($criteria);

		if (! self::model()->exists('team_id=:teamId AND question_id=:questionId', array('teamId' => $team->id, 'questionId' => $questionId))) {
			$ta = new TeamAnswer();
			$ta->team_id = $team->id;
			$ta->question_id = $questionId;
			$ta->tour_num = $tourNum;
			$ta->save();
			echo 'ok';
		}
		else {
			echo 'fail';
		}
	}
	
	public function commitAnswers($questionId)
	{
		$tr = Yii::app()->db->beginTransaction();

		$tas = TeamAnswer::model()->findAllByAttributes(array(
			'commited' => 0,
			'question_id' => $questionId
		));

		foreach ($tas as $ta) {
			$ta->commited=1;
			$ta->save();
			Team::model()->updateCounters(
				array('points' => 1), 
				'id=:id', 
				array('id'=>$ta->team_id)
			);			
		}

		RuntimeData::model()->setData(array(
			'last_update' => time()
		));
		$tr->commit();		
	}
}
