<?php

class RuntimeController extends CController
{
	public $breadcrumbs = array(
		'Управление игрой'
	);
	public $tourCount;
	public $questionInTourCount;

	public function actionIndex()
	{
		$runtimeData = RuntimeData::model()->getData();
		$commitTour = Yii::app()->request->getParam('commitTour', $runtimeData->current_tour);
		$teamsData = Team::model()->getAllWithResults($commitTour, false, Team::ORDER_NAME );
		//$photoAnswers = PhotoAnswer::model()->getPhotos ( $runtimeData->current_folder );

		$this->tourCount = $runtimeData->tour_count;
		$this->questionInTourCount = $runtimeData->question_in_tour_count;

		$this->render('gamePage', array(
			'commitTour' => $commitTour,
			'commitQuestion' => $runtimeData->current_question,
			'teamsData' => $teamsData,
			'showTour' => $runtimeData->current_tour,
			'tourCount' => $this->tourCount,
			'questionInTourCount' => $this->questionInTourCount,
			//'photoAnswers' => $photoAnswers,
			'sort' => Yii::app()->request->getParam('sort', 0),
		));
	}

	public function actionReset()
	{
		$this->render('resetPage');
	}

	public function actionSetTour()
	{
		RuntimeData::model()->setData(array(
			'last_update' => time(),
			'current_tour' => Yii::app()->request->getParam('tourNum', 1),
			'current_question' => 1
		));
		echo 'ok';
	}

	public function actionSetQuestion()
	{
		RuntimeData::model()->setData(array(
			'current_question' => Yii::app()->request->getParam('questionNum', 1)
		));
		echo 'ok';
	}
}