<?php

class ResultsController extends CController
{
	public $layout = 'results';

	public $scripts = [ 'results.js' ];
	
	public $tourCount;
	public $questionInTourCount;
	public $imgPath;

	public function beforeAction($action) {
		if( parent::beforeAction($action) ) {
			
			$runtimeData = RuntimeData::model()->getData();
			if ( '' != $runtimeData->theme )
			{
				\Yii::app()->setTheme ( $runtimeData->theme );
			}
			
			$cs = \Yii::app()->clientScript;
			$theme = \Yii::app()->theme;
			$cs->registerCssFile ( $theme->getBaseUrl() . '/css/results.css');
			$cs->registerScriptFile ( \Yii::app()->getBaseUrl() . '/js/results.js');
			return true;
		}
		return false;
	}

	public function actionIndex()
	{
		$runtimeData = RuntimeData::model()->getData();
		$this->tourCount = $runtimeData->tour_count;
		$this->questionInTourCount = $runtimeData->question_in_tour_count;
		$this->imgPath = \Yii::app()->theme->getBaseUrl().'/images/';
		$this->renderText('');
	}

	public function actionTotal()
	{
		$runtimeData = RuntimeData::model()->getData();
		$this->tourCount = $runtimeData->tour_count;
		$this->questionInTourCount = $runtimeData->question_in_tour_count;
		$this->imgPath = \Yii::app()->getBaseUrl().'/images/';
		$this->renderText('!');
	}

	public function actionLongdata()
	{
		$clientUpdateTime = Yii::app()->request->getParam('time', 0);
		$runtimeData = RuntimeData::model()->getData();
		
	}
		
	public function actionData()
	{
		$clientUpdateTime = Yii::app()->request->getParam('time', 0);
		$runtimeData = RuntimeData::model()->getData();
		
		if ( 'video' === $runtimeData->displaycmd )
		{
			$runtimeData->displaycmd = null;
			$runtimeData->save ();
			echo json_encode(array(
				'cmd' => 'video',
				'parm' => $runtimeData->displayparm
			));
		}
		else if ( 'image' === $runtimeData->displaycmd )
		{
			$runtimeData->displaycmd = null;
			$runtimeData->save ();
			echo json_encode(array(
				'cmd' => 'image',
				'parm' => $runtimeData->displayparm
			));
		}
		else if ( 'results' === $runtimeData->displaycmd )
		{
			$runtimeData->displaycmd = null;
			$runtimeData->save ();
			echo json_encode(array(
				'cmd' => 'results',
				'parm' => true
			));
		}
		else if ( 'simplecountdown' === $runtimeData->displaycmd )
		{
			$runtimeData->displaycmd = null;
			$runtimeData->save ();
			echo json_encode(array(
				'cmd' => 'simplecountdown',
				'parm' => $runtimeData->displayparm
			));
		}
		else if ($runtimeData->last_update <= $clientUpdateTime) {
			echo json_encode(array(
				'cmd' => 'none',
				'parm' => false
			));
		}
		else {
			$data = Team::model()->getAllWithResults($runtimeData->current_tour);			
			echo json_encode(array(
				'cmd' => 'update',
				'parm' => true,
				'time' => $runtimeData->last_update,
				'tourNum' => $runtimeData->current_tour,
				'data' => $data,
			));
		}
	}
	
	private function createFile( $runtimeData )
	{
		$data = Team::model()->getAllWithResults();
		$file = '';
		for ($tour = 0; $tour < $runtimeData->tour_count; $tour++ ) {
			foreach ($data as $teamData) {
				$file .= $teamData['rating_id'].';';
				$teamData['name'] = str_replace('"', '""', $teamData['name']);
				$teamData['city'] = str_replace('"', '""', $teamData['city']);
			
				$file .= '"'.$teamData['name'].'";';
				$file .= '"'.$teamData['city'].'";';
				$file .= $tour+1;
				for ($q=1; $q <= $runtimeData->question_in_tour_count; $q++) {
					$qq = $q + $tour * $runtimeData->question_in_tour_count;
					if ( isset($teamData['tour_answers'][$qq]['commited'] ) )
					{
						$file .= ';1';
					}
					else if ( isset($teamData['tour_arguable_answers'][$qq]['commited'] ) )
					{
						$aatext = $teamData['tour_arguable_answers'][$qq]['text'];
						if ( '' != $aatext )
						{
							$file .= ';"'.str_replace('"', '""', $aatext ).'"';
						}
						else
						{
							$file .= ';?';
						}
					}
					else
					{
						$file .= ';0';
					}
				}
				$file .= "\n";
			}
		}
		return mb_convert_encoding ( $file, 'CP1251', 'UTF-8' );
	}

	public function actionSaveFile()
	{
		$this->actionSaveFileSilent();
		echo 'ok';
	}
	public function actionSaveFileSilent()
	{
		$runtimeData = RuntimeData::model()->getData();
		$filename = date('YmdHis').'-Results.csv';
		$dir = Yii::app()->basePath.'/../upload/'.$runtimeData->current_folder;
		if (is_dir($dir) )
		{
			file_put_contents ( $dir.'/'.$filename, $this->createFile( $runtimeData ) );
		}
	}

	public function actionDownload()
	{
		$runtimeData = RuntimeData::model()->getData();
		$filename = date('YmdHis').'-Results.csv';
		header('Content-Disposition:attachment;filename='.$filename);
		echo $this->createFile( $runtimeData );
	}
	public function actionDownloadAndSaveFile()
	{
		$runtimeData = RuntimeData::model()->getData();
		$filename = date('YmdHis').'-Results.csv';
		header('Content-Disposition:attachment;filename='.$filename);
		file_put_contents ( Yii::app()->basePath.'/../upload/'.$runtimeData->current_folder.'/'.$filename, $this->createFile( $runtimeData ) );
		echo $this->createFile( $runtimeData );
	}

	public function actionPublish()
	{
		PublishHelper::get($_POST);
	}
}