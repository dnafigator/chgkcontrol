<?php

class PhotoAnswerController extends CController
{
	public $layout = 'photoAnswer';
	public $tourCount;
	public $questionInTourCount;

	public function actionIndex()
	{
		$runtimeData = RuntimeData::model()->getData();
		$this->tourCount = $runtimeData->tour_count;
		$this->questionInTourCount = $runtimeData->question_in_tour_count;
		$question = isset ( $_GET [ 'question' ] ) ? $_GET [ 'question' ] : 0;

		$answers = Team::model()->getAllWithResults( null, false );
		$imgs = PhotoAnswer::model()->getPhotos ( $runtimeData->current_folder, $question );

		$this->render( 'photoAnswer', array(
			'currentQuestion' => $question,
			'tourCount' => $this->tourCount,
			'questionInTourCount' => $this->questionInTourCount,
			'currentFolder' => $runtimeData->current_folder,
			'answers' => $answers,
			'images' => $imgs
		));
	}
}