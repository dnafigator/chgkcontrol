<?php

class SiteController extends CController
{
	public $scripts = array(
		'site.js'
	);

	public function actionIndex()
	{
		$tm = new CThemeManager ();
        $runtimeData = RuntimeData::model()->getData();
		
		$themes = [['name' => '', 'value' => '', 'default' => false ]];
		foreach ( $tm->themeNames as $themeName )
		{
			$themes[] = ['name' => $themeName, 'value' => $themeName, 'default' => $themeName == $runtimeData->theme ];
		}
		
		\Yii::app()->setTheme ( $runtimeData->theme );
                
		$this->render('index', array(
			'themes' => $themes
		));
	}


	private $ignoreItems = array (
		'assets',
		'nbproject',
		'protected/runtime',	
		'upload',
	);

	private function getFolderLastChanges ( $name )
	{
		$time = 0;
		if ( $f = opendir ( $name ) )
		{
			while ( $fname = readdir ( $f ) )
			{
				$tm = 0;
				$compare = substr ( $name.'/'.$fname, strlen ( yii::app()->basePath ) + 4 );
				if (in_array ( $compare, $this->ignoreItems ) )
				{
					continue;
				}
				if ( is_file ( $name.'/'.$fname ) )
				{
					$tm = filemtime ( $name.'/'.$fname );
				}
				else if ( is_dir ( $name.'/'.$fname ) )
				{
					if ( ('.' != $fname) && ( '..' != $fname ) )
					{
						$tm = $this->getFolderLastChanges ( $name.'/'.$fname );
					}
				}
				if ( $tm > $time )
				{
					$time = $tm;
				}
			}
		}
		return $time;
	}
	private function getLastChanges ()
	{
		return $this->getFolderLastChanges (yii::app ()->basePath.'/..');
	}

	public function actionInfo()
	{
		$datetime = date ( 'Y-m-d H:i:s', $this->getLastChanges () );
		$this->render('info', array(
			'LastChanges' => $datetime
		));
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else {
				echo '<pre>';
				print_r($error);
			}	
		}
	}
	public function actionSetTheme()
        {
            $runtimeData = RuntimeData::model()->getData();
            $newTheme = Yii::app()->request->getParam('theme');
            if ( $runtimeData->theme != $newTheme )
            {
                $runtimeData->theme = $newTheme;
                $runtimeData->save ();
                \Yii::app()->setTheme ( $newTheme );
            }
        }
        
}