<?php

class RegistrationController extends CController
{
	private $CSVseparator = ';';
	public $breadcrumbs = array(
		'Регистрация'
	);

	public function actionIndex()
	{
		$model = new Team();
		$model->highlight = '';
		$filter = Yii::app()->request->getParam('Team');
		if ($filter) {
			$model->setAttributes($filter);
		}

		if (isset($_GET['ajax'])) {
			$this->renderPartial('teamsReg', array(
				'model' => $model,
			));
		}
		else {
			$this->render('teamsReg', array(
				'model' => $model,
			));
		}
	}

	public function actionDelete()
	{
		Team::model()->deleteByPk(Yii::app()->request->getParam('id'));
	}
	public function actionLoad()
	{

	}
	public function actionSave()
	{

	}

	public function teamCreate($data)
	{
		$model = new Team();
		if ($data) {
			$model->setAttributes($data);
			$runtimeData = RuntimeData::model()->getData();
			$model->game_id = $runtimeData->current_game;
			if ($model->validate()) {
				$model->save(false);
				RuntimeData::model()->setData(array(
					'last_update' => time()
				));
				return true;
			}
			else {
				print_r($model);
				$gridModel = new Team();
				$this->render('teamsReg', array(
					'model' => $model,
					'gridModel' => $gridModel
				));
				return false;
			}
		}
		else {
			return true;
		}
	}


	public function actionCreate()
	{
		$data = Yii::app()->request->getParam('Team');
		if ( $this->teamCreate($data) )
		{
			$this->redirect($this->createAbsoluteUrl('registration/index'));
		}
	}


	public function actionUpdate()
	{
		$team = Team::model()->findByPk(Yii::app()->request->getParam('pk'));
		if ($team) {
			$team->setAttribute(Yii::app()->request->getParam('name'), Yii::app()->request->getParam('value'));
			if (!$team->save()) {
				header("HTTP/1.1 501");
				$error = $team->getError(Yii::app()->request->getParam('name'));
				echo $error;
			}

		}
	}


	public function actionDownload()
	{
		$data = Team::model()->getAllWithResults();
		$runtimeData = RuntimeData::model()->getData();
		header('Content-Disposition:attachment;filename='.date('YmdHis').'-TeamList.csv');
		$file = '';

		$file .= '"Номер";"ID команды";"Название";"Город";"ЭКР"'."\n";

		foreach ($data as $teamData) {
			$file .= '"'.$teamData['num'].'"'.$this->CSVseparator;
			$file .= '"'.$teamData['rating_id'].'"'.$this->CSVseparator;

			$teamData['name'] = str_replace('"', '""', $teamData['name']);
			$teamData['city'] = str_replace('"', '""', $teamData['city']);

			$file .= '"'.$teamData['name'].'"'.$this->CSVseparator;
			$file .= '"'.$teamData['city'].'"'.$this->CSVseparator;
			$file .= '"'.$teamData['highlight'].'"'.$this->CSVseparator;
			$file .= "\n";
		}

		echo mb_convert_encoding ( $file, 'CP1251', 'UTF-8' );
	}

	private function getCSV( $f )
	{
		$line = fgets ($f);
		if (!$line )
		{
			return 0;
		}
		$line = mb_convert_encoding ( $line, 'UTF-8', 'CP1251' );
		$ln = str_getcsv ( $line, $this->CSVseparator, '"');
		return $ln;
	}

	public function actionUpload()
	{

		$model = Team::model();
		$model->setScenario ( 'fileUpload' );
		$model->teamListFile=CUploadedFile::getInstance($model,'teamListFile');
		if ( $model->teamListFile )
		{
			if ( $f = fopen ( $model->teamListFile->tempName, 'r' ) )
			{
				$ln = $this->getCSV ( $f );
				if ( $ln && ('Номер' == $ln [ 0 ]) && ('ID команды' == $ln [ 1 ]) && ('Название' == $ln [ 2 ]) && ('Город' == $ln [ 3 ]) && ('ЭКР' == $ln [ 4 ]) )
				{
					$criteria = new CDbCriteria();
                    $criteria->join = 'JOIN chgk_runtime_data ON chgk_runtime_data.current_game=game_id';
					Team::model()->deleteAll($criteria);
					while ( $ln = $this->getCSV ( $f ) )
					{
						$data = array ( "num" => $ln[0], "rating_id" => $ln[1], "name" => $ln[2], "city" => $ln[3], "highlight" => $ln[4] );
						$this->teamCreate ( $data );
					}
				}
				else
				{
					//TODO: Обработать ошибку
					echo "Неверный формат CSV";
					return;
				}
			}
		}
		$this->redirect($this->createAbsoluteUrl('registration/index'));
	}
}