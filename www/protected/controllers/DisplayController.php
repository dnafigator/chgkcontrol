<?php
class DisplayController extends CController
{
	public $breadcrumbs = array(
		'Управление изображением'
	);
	public $video;
	public $scripts = ['displaycontrols.js'];
	public $styles = ['displaycontrols.css'];
	
	public function actionIndex()
	{
		$runtimeData = RuntimeData::model()->getData();
		if ( '' != $runtimeData->theme )
		{
			\Yii::app()->setTheme ( $runtimeData->theme );
		}
		
		$this->render ( 'index' );
	}
	public function actionVideo()
	{
		$runtimeData = RuntimeData::model()->getData();
		if ( '' != $runtimeData->theme )
		{
			\Yii::app()->setTheme ( $runtimeData->theme );
		}
		$runtimeData = RuntimeData::model()->getData();
		$runtimeData->displaycmd = 'video';
		$runtimeData->displayparm = \Yii::app()->theme->getBaseUrl().'/display/'.\Yii::app()->request->getParam('file');
		$runtimeData->save ();
	}
	
	public function actionResults()
	{
		$runtimeData = RuntimeData::model()->getData();
		if ( '' != $runtimeData->theme )
		{
			\Yii::app()->setTheme ( $runtimeData->theme );
		}
		
		$runtimeData = RuntimeData::model()->getData();
		$runtimeData->displaycmd = 'results';
		print_r ( \Yii::app()->theme );
		$runtimeData->save ();
	}
	
	public function actionImage()
	{
		$runtimeData = RuntimeData::model()->getData();
		if ( '' != $runtimeData->theme )
		{
			\Yii::app()->setTheme ( $runtimeData->theme );
		}
		
		
		$runtimeData = RuntimeData::model()->getData();
		$runtimeData->displaycmd = 'image';
		$runtimeData->displayparm = \Yii::app()->theme->getBaseUrl().'/display/'.\Yii::app()->request->getParam('file');
		echo $runtimeData->displayparm;
		$runtimeData->save ();
	}
	
	public function actionSimplecountdown()
	{
		$runtimeData = RuntimeData::model()->getData();
		$runtimeData->displaycmd = 'simplecountdown';
		$runtimeData->displayparm = \Yii::app()->request->getParam('file');
		$runtimeData->save ();
	}
	
	
}
