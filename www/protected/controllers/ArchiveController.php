<?php

class ArchiveController extends CController
{
        var $uploadFolder;
        var $arguabeFiles, $resultFiles;
        var $teams;
	public $breadcrumbs = array(
		'Архив'
	);
        
        private function makeFolderNameHumanReadable ( $fn )
        {
            return substr($fn, 0, 4).'-'.substr($fn, 4, 2).'-'.substr($fn, 6, 2).' '.substr($fn, 8, 2).':'.substr($fn, 10, 2).':'.substr($fn, 12, 2);
        }
        
        public function actionIndex()
        {
            $folder = Yii::app()->request->getParam('folder');
            $this->uploadFolder = Yii::app()->basePath.'/../upload/';
            
            
            $this->teams = array ();
            $tms = Team::model()->getAllWithResults();
            foreach ( $tms as $tm )
            {
                $this->teams[ $tm['id']] = $tm['name'];
            }
            
            if ( !$folder )
            {
                $this->makeTopFolder();
            }
            else
            {
                $this->makeInnerFolder ( $folder = Yii::app()->request->getParam('folder') );
            }
            
        }
    
        private function makeTopFolder()
        {
                $list = array ();
                $dir = opendir ( $this->uploadFolder );
                if ( $dir )
                {
                    while ( $line = readdir ( $dir ) )
                    {
                        if ( is_dir ( $this->uploadFolder.'/'.$line ) && ('.' !== $line) && ('..' !==$line) )
                        {
                            $list[] = array ( 'folder' => $line, 'name' => $this->makeFolderNameHumanReadable ( $line ) );
                        }
                    }
                }
                $this->render('archiveTop', array(
                    'folders' => $list,
        	));						
        }
        
        private function knownFile ( $fn )
        {
            if (preg_match('/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})-Results.csv/', $fn, $m ))
            {
                $this->resultFiles[] = array ( 'file' => $fn, 'name' => 'Результаты '.$m[1].'-'.$m[2].'-'.$m[3].' '.$m[4].':'.$m[5].':'.$m[6] );
            }
            elseif (preg_match('/([0-9]+?)_([0-9]+?)_([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2}).jpg/', $fn, $m ))
                {
                    $this->arguabeFiles[] = array ( 'file' => $fn, 'name' => 'Спорный ответ команды '.$m[1].' &laquo;'.$this->teams [ $m[1] ].'&raquo; на вопрос '. $m[2] );
                }
            return false;
        }
        
        private function makeInnerFolder( $fn )
        {
                $this->arguabeFiles = array ();
                $this->resultFiles = array ();
                $list = array ();
                $dir = opendir ( $this->uploadFolder.'/'.$fn );
                if ( $dir )
                {
                    while ( $line = readdir ( $dir ) )
                    {
                        if ( is_file ( $this->uploadFolder.'/'.$fn.'/'.$line ) )
                        {
                            $this->knownFile ( $line );
                        }
                    }
                }
        	$list = array_merge ( $this->resultFiles, $this->arguabeFiles );
                $this->render('archiveInside', array(
                    'folder' => $fn,
                    'title' => $this->makeFolderNameHumanReadable ( $fn ),
                    'files' => $list
        	));						
            
        }

        public function actionDownload()
        {
            $this->uploadFolder = Yii::app()->basePath.'/../upload/';
            $folder = Yii::app()->request->getParam('folder');
            $file = Yii::app()->request->getParam('file');
            
            $fn = $this->uploadFolder."/$folder/$file";
            if ( $f = fopen ( $fn, 'r' ) )
            {
                header('Content-Disposition:attachment;filename='.$file);
                fpassthru($f);
            }
        }
        
        
}
