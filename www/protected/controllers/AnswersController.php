<?php

class AnswersController extends CController
{


	public function actionAdd()
	{

		$runtimeData = RuntimeData::model()->getData ();
		//$tourCount = $runtimeData->tour_count;
		$questionInTourCount = $runtimeData->question_in_tour_count;

		$questionId = Yii::app()->request->getParam('questionId');
		$teamId = Yii::app()->request->getParam('teamId');

		$tourNum = (int)(($questionId-1) / $questionInTourCount) + 1;

		TeamAnswer::model()->addAnswer($teamId, $questionId, $tourNum);
	}

	public function actionAddArguable()
	{
		$runtimeData = RuntimeData::model()->getData ();
		$questionInTourCount = $runtimeData->question_in_tour_count;
		$questionId = Yii::app()->request->getParam('questionId');
		$teamId = Yii::app()->request->getParam('teamId');
		$text = Yii::app()->request->getParam('text');
		TeamArguableAnswer::model()->addArguableAnswer($teamId, $questionId, $text);
		echo 'ok';
	}

	public function actionCommit()
	{
		$questionId = Yii::app()->request->getParam('questionId');
		TeamAnswer::model()->commitAnswers($questionId);
		TeamArguableAnswer::model()->commitAnswers($questionId);
		PublishHelper::publish();
		echo 'ok';
	}

	public function actionDelete()
	{
		$questionId = Yii::app()->request->getParam('questionId');
		$teamNum = Yii::app()->request->getParam('teamId');

		$criteria = new CDbCriteria();
		$criteria->join = 'JOIN chgk_runtime_data ON chgk_runtime_data.current_game=game_id';
		$criteria->addColumnCondition(array('num'=>$teamNum));
		$team = Team::model()->find($criteria);


		$ta = TeamAnswer::model()->findByAttributes(array(
			'question_id' => $questionId,
			'team_id' => $team->id
		));
		if ($ta) {
			if ($ta->commited) {
				Team::model()->updateCounters(array('points' => -1), 'id=:id', array('id' => $team->id));
			}
			$ta->delete();
		}

		$taa = TeamArguableAnswer::model()->findByAttributes(array(
			'question_id' => $questionId,
			'team_id' => $team->id
		));
		if ($taa) {
			$taa->delete();
		}

		if ( $ta || $taa )
		{
			RuntimeData::model()->setData(array('last_update' => time()));
		}

		echo 'ok';
	}

	public function actionDeleteAll()
	{
		$criteria = new CDbCriteria();
		$criteria->join = 'JOIN chgk_runtime_data ON chgk_runtime_data.current_game=game_id';
		TeamAnswer::model()->deleteAll($criteria);
		Team::model()->updateAll($criteria, array('points' => 0));
		RuntimeData::model()->setData(array(
			'current_game' => null,
			'current_tour' => 1,
			'current_question' => 1,
			'last_update' => time()
		));
		$this->redirect($this->createAbsoluteUrl('runtime/index'));
	}
}