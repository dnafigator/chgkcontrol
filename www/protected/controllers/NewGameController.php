<?php
class NewGameController extends CController
{

	public $breadcrumbs = array(
		'Новая игра'
	);

	public function actionIndex ()
	{
		$runtimeData = RuntimeData::model()->getData ();
		$games = Game::model()->findAll();
		$this->render ( 'newGame', array (
			'tourCount' => $runtimeData->tour_count,
			'questionInTourCount' => $runtimeData->question_in_tour_count,
			'current_id' => $runtimeData->current_game,
			'current_name' => $runtimeData->game_name,
			'games' => $games,
		) );
	}

	public function actionPublish () {
		$this->breadcrumbs = array( 'Публикация результатов' );
		$runtimeData = RuntimeData::model()->getData ();

		if(\yii::app()->request->getPost('save')) {
			$runtimeData->publish_game = Yii::app()->request->getPost('show');
			$runtimeData->publish_url = Yii::app()->request->getPost('url');
			$runtimeData->publish_key = Yii::app()->request->getPost('pwd');
			$runtimeData->save();
			\yii::app()->user->setFlash('txt', 'Сохранено');
			$this->refresh();
			return;
		}
		$this->render ( 'publish', array (
			'show' => $runtimeData->publish_game,
			'url' => $runtimeData->publish_url,
			'pwd' => $runtimeData->publish_key,
			'txt' => \yii::app()->user->getFlash('txt'),
		) );
	}

	public function actionNewGame()
	{
		$tourCount = Yii::app ()->request->getParam( 'tour_count', 3 );
		$questionInTourCount = Yii::app ()->request->getParam( 'question_in_tour_count', 12 );
		$gameName = Yii::app ()->request->getParam( 'game_name', 'Игра '.date('Y-m-d H:i:s') );

		$currentFolder = date ('YmdHis');
		mkdir ( Yii::app()->basePath.'/../upload/'.$currentFolder );

		$game = new Game();
		$game->name = $gameName;
		$game->tour_count = $tourCount;
		$game->question_in_tour_count = $questionInTourCount;
		$game->current_folder = $currentFolder;
		$game->save();
		return $this->actionSelectGame($game->id);
	}

	public function actionSelectGame($game_id)
	{
		$game = Game::model()->findByPk($game_id);
		RuntimeData::model()->setData(array(
			'current_game' => $game->id,
			'game_name' => $game->name,
			'current_tour' => 1,
			'current_question' => 1,
			'last_update' => time(),
			'tour_count' => $game->tour_count,
			'question_in_tour_count' => $game->question_in_tour_count,
			'current_folder' => $game->current_folder,
		));
		$this->redirect($this->createAbsoluteUrl('registration/index'));
	}
}
