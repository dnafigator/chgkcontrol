<?php

class ResultsTableController extends CController
{
	public $layout = 'resultsTable';

	public $tourCount;
	public $questionInTourCount;

	public function actionIndex()
	{
		$this->actionSimpleTable ();
	}

	public function actionSimpleTable ()
	{
		$orderBy = Team::ORDER_POINTS;
		if ( isset ( $_GET [ 'name' ] ) )
		{
			$orderBy = Team::ORDER_NAME;
		}
		else if ( isset ( $_GET [ 'id' ] ) )
		{
			$orderBy = Team::ORDER_ID;
		}
		$runtimeData = RuntimeData::model()->getData();
		$teamsData = Team::model()->getAllWithResults(null, false, $orderBy );

		$this->tourCount = $runtimeData->tour_count;
		$this->questionInTourCount = $runtimeData->question_in_tour_count;

		$this->render('simpleTable', array(
			'teamsData' => $teamsData,
			'tourCount' => $this->tourCount,
			'questionInTourCount' => $this->questionInTourCount
		));
	}

	public function actionTeamTable ()
	{
		$runtimeData = RuntimeData::model()->getData();
		$teamsData = Team::model()->getTeamResults();

		$this->tourCount = $runtimeData->tour_count;
		$this->questionInTourCount = $runtimeData->question_in_tour_count;

		$this->render('teamTable', array(
			'teamsData' => $teamsData,
			'tourCount' => $this->tourCount,
			'questionInTourCount' => $this->questionInTourCount
		));
	}

	public function actionPublished ()
	{
		$orderBy = Team::ORDER_POINTS;
		if ( isset ( $_GET [ 'name' ] ) )
		{
			$orderBy = Team::ORDER_NAME;
		}
		else if ( isset ( $_GET [ 'id' ] ) )
		{
			$orderBy = Team::ORDER_ID;
		}
		$runtimeData = RuntimeData::model()->getData();
		$data = PublishedResults::model()->findByPk($runtimeData->publish_game);
		if($data) {
			$this->render('simpleTable', array(
				'teamsData' => json_decode($data->data, true),
				'tourCount' => $data->tour_count,
				'questionInTourCount' => $data->q_in_tour_count,
			));
		}
	}

}