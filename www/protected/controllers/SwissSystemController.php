<?php

class SwissSystemController extends CController
{
    public $breadcrumbs = array(
        'Швейцарка'
    );
    
    private function td()
    {
        echo '<pre>';
        foreach($this->teamData as $td) echo $td['id'].' ';
        //die;
    }
    public function actionIndex ()
    {
        //echo "<pre>";
        $runtimeData = RuntimeData::model()->getData ();
        $tourCount = $runtimeData->tour_count;
        $questionInTourCount = $runtimeData->question_in_tour_count;

        $qs = $tourCount*$questionInTourCount;
        
        $te = Yii::app()->request->getParam('edges', '1 8 15 22 29 36 43');
        
        preg_match_all('/[0-9]+/', $te, $m);
        
        $this->gameEdges = [];
        if(isset($m[0])) {
            foreach($m[0] as $e ) {
                $this->gameEdges[] = $e;
            }
        }
        $this->gameEdges[] = $qs + 1;
        
        $this->teamInfo = Team::model()->getAllWithResults();
        $this->setInitialRating();
        $teamI = [];

        foreach ($this->teamInfo as $key => $val) {
            $teamI[$val['id']] = $val;
            $o = [
                'id' => $val['id'],
                'swiss-score' => $val['rating'],
            ];
            for($i = 1; $i <= $qs; $i ++ ) {
                $o['result'][$i] = isset($val['tour_answers'][$i]) && 1 == $val['tour_answers'][$i]['commited'] ? 1 : 0;
            }
            $this->teamData[$val['id']] = $o;
        }
        $this->getEdgeResults();
        foreach ($this->teamData as $key => $val) {
            $teamI[$val['id']]['score'] = $this->teamData[$key]['swiss-total'];
            unset($this->teamData[$key]['result'] );
        }
//        $this->td();

        $this->countSwissTournament();
        //print_r($this->teamData);
        $this->render ('index', ['te' => $te, 'ri' => $this->roundInfo, 'ti' => $teamI, 'log' => $this->_log]);
    }

    private $playedPairs = [];
    private $initial = [];
    private $gameEdges = [];
    private $teamInfo = [];
    private $teamData = [];
    private $roundInfo = [];
    
    private function played($i1, $i2) {
        foreach ($this->playedPairs as $v) {
            if ( $v[0] == $this->teamData[$i1]['id'] && $v[1] == $this->teamData[$i2]['id'] ||
                $v[0] == $this->teamData[$i2]['id'] && $v[1] == $this->teamData[$i1]['id'] )
            {
                return true;
            }
        }
        return false;
    }

    private function countSwissTournament()
    {
        foreach($this->gameEdges as $r => $e) {
            $this->playSwissRound($r+1, $r == sizeof($this->gameEdges) - 1);
        }
        //print_r ($this->roundInfo);
        //print_r ($this->groups);
    }

    private function getEdgeResults($cur = -1) {
        $from = -1;
        $tour = 0;
        foreach($this->gameEdges as $q) {
            if ( -1 != $from ) {
                foreach ($this->teamData as $key => $value) {
                    $this->teamData[$key]['swiss-round'][$tour] = 0;
                    for ($i = $from; $i < $q; $i++) {
                        if ( $this->teamData[$key]['result'][$i]) {
                            $this->teamData[$key]['swiss-round'][$tour]++;
                        }
                    }
                    $this->teamData[$key]['swiss-total'][$tour] = $this->teamData[$key]['swiss-round'][$tour] + ((1 == $tour) ? 0 : $this->teamData[$key]['swiss-total'][$tour-1]);
                }
            }
            $from = $q;
            $tour ++;
            if(-1 !== $cur && $cur < $tour) {
                break;
            }
        }
    }

    private $groups = [];
    private function findPair($played_id, $n, $s)
    {
        $spare = null;
        $found = false;
        for($gi = $n; $gi < sizeof($this->groups) && !$found; $gi ++ ) {
//            echo "group $gi, ".sizeof($this->groups[$gi]['teams'])." teams<br>";
            $start = ($gi == $n) ? $s : 0;
            for($i = $start; $i < sizeof($this->groups[$gi]['teams']); $i ++ ) {
//                echo "item $i<br>";
                $it = $this->groups[$gi]['teams'][$i];
                if ( !$it['spare'] ) {
                    continue;
                }
                if ( !$spare ) {
                    $spare = ['gi' => $gi, 'i' => $i];
                }
                if (-1 == $played_id) {
                    $found = true;
                    break;
                } else if ( !$this->played($played_id, $it['id']) ) {
                    $this->playedPairs[] = [$this->teamData[$played_id]['id'], $this->teamData[$it['id']]['id']];
                    $spare = ['gi' => $gi, 'i' => $i];
                    $found = true;
                    break;
                } else {
                    //already played
                    $this->log($this->teamData[$played_id]['id'].' и '.$this->teamData[$it['id']]['id'].' уже играли, ищем другую пару.<br>');
                }
            }
        }
        if($spare) {
            $this->groups[$spare['gi']]['teams'][$spare['i']]['spare'] = false;
            return $this->groups[$spare['gi']]['teams'][$spare['i']]['id'];
        } else {
            return -1;
        }
    }
        
    private $_log = '';
    private function log($l)
    {
        $this->_log .= $l;
    }
    private function playSwissRound($round, $after_last = false)
    {
        $this->log($after_last ? '<b>Результат</b><br/>' : '<b>Раунд '.$round.'</b><br/>');
        //print_r($this->teamData);
        
        usort($this->teamData, function($a,$b) use ($round){
            if($b['swiss-score'] != $a['swiss-score']){
                return $b['swiss-score'] - $a['swiss-score'];
            } else if (1 == $round) {
                return 0;
            } else {
                //echo $round."\n";
                //print_r($a);
                //print_r($b);
                //echo $b['id'].' '.$b['swiss-round'][$round].':'.$a['swiss-round'][$round].' '.$a['id']."<br>\n";
                if ( $b['swiss-round'][$round-1] != $a['swiss-round'][$round-1] ) {
                    return $b['swiss-round'][$round-1] - $a['swiss-round'][$round-1];
                } else {
                    return 0;
                }
            }
        });
        
        $this->log('Рейтинг команд<br/>');
        foreach ($this->teamData as $key => $val) {
            $this->log ( "\t".$val['id'].' '.$val['swiss-score'].'<br>');
        }
        
        if(1 == $round) {
            foreach ($this->teamData as $key => $val) {
                $this->teamData [$key]['rating'] = $this->teamData [$key]['swiss-score'];
                $this->teamData [$key]['swiss-score'] = 0;
            }
        }

        $this->roundInfo[$round] = [];

        //make groups
        $curGroup = -1;
        $curScore = -1;
        foreach ($this->teamData as $key => $val) {
            if ( $curScore != $val['swiss-score']) {
                $curGroup ++;
                $curScore = $val['swiss-score'];
                $this->groups[$curGroup] = ['teams' => [], 'score' => $curScore];
            }
            $this->groups [$curGroup]['teams'][] = [
                'id' => $key,
                'spare' => true,
                //'team' => &$this->teamData[$key],
            ];
            $this->teamData[$key]['swiss-group'] = $curGroup;
            $score = $val['swiss-score'];
            if ( 1 == $round ) {
                $score = $val['rating'];
                unset($this->teamData['rating']);
            }
            $this->roundInfo[$round]['teams'][] = ['id' => $val['id'], 'score' => $score];
        }
        
        if($after_last) {
            return;
        }
        
        //if odd, drop last of group down, except for the last group
        $restart = false;
        do {
            $restart = false;
            foreach ($this->groups as $key => $val) {
                if(!isset($this->groups[$key+1])) {
                    break;
                }
                if ( sizeof($val['teams']) % 2 ) {
                    $team = array_pop($this->groups[$key]['teams']);
                    array_unshift($this->groups[$key+1]['teams'], $team);
                    $this->log('Команда '.$this->teamData[$team['id']]['id'].' перенесена в следующую группу (нечётное количество в группе)<br>');
                    $restart = true;
                    break;
                }
            }
        } while($restart);
        $this->log('Группы<br/>');
        foreach($this->groups as $k => $g) {
            $this->log($k.': ');
            foreach($g['teams'] as $gi) {
                $this->log($this->teamData[$gi['id']]['id'].' ');
            }
            $this->log("<br/>");
        }
        
        //make pairs
        foreach ($this->groups as $key => $val) {
            for(;;) {
                $first = $this->findPair(-1, $key, 0);
                $second = $this->findPair($first, $key, floor(sizeof($val['teams'])/2));
                
                if ( -1 == $first && -1 == $second ) {
                    break;
                }
                //play!
                $s1 = $this->teamData[$first]['swiss-round'][$round];
                $s2 = 'x';
                $secondName = '+';
                if (-1 != $second) {
                    $s2 = $this->teamData[$second]['swiss-round'][$round];
                    $secondName = $this->teamData[$second]['id'];
                } else if(isset($this->teamData[$first]['ll'])) {
                    $secondName = '-';
                }
                
                $this->roundInfo[$round]['games'][] = [
                    'first' => [
                        'id' => $this->teamData[$first]['id'], 
                        'result' => $s1,
                        ],
                    'second' => [
                        'id' => $secondName, 
                        'result' => $s2,
                        ]
                    ];
                
                if( -1 == $second ) {
                    if(!isset($this->teamData[$first]['ll'])) {
                        $this->teamData[$first]['ll'] = true;
                        $this->teamData[$first]['swiss-score'] += 2;
                    }
                } else if ( $s1 > $s2 ) {
                    $this->teamData[$first]['swiss-score'] += 2;
                } else if ( $s1 < $s2 ) {
                    $this->teamData[$second]['swiss-score'] += 2;
                } else {
                    $this->teamData[$first]['swiss-score'] += 1;
                    $this->teamData[$second]['swiss-score'] += 1;
                }
            }
        }
        $this->log('Игры<br/>');
        foreach( $this->roundInfo[$round]['games'] as $ri ) {
            $this->log($ri['first']['id'].' '. $ri['first']['result'].':'.$ri['second']['result'].' '.$ri['second']['id'].'<br/>');
        }
        
        $this->log('<br/>');
    }
    
    private function setInitialRating()
    {
        $ratings = SwissRating::model()->findAll();
        foreach ($this->teamInfo as $key => $team) {
            $this->teamInfo[$key]['rating'] = 100000-$team['id'];
            foreach ($ratings as $rating) {
                if ( $team['id'] == $rating->id ) {
                    $this->teamInfo[$key]['rating'] = $rating->rating;
                    break;
                }
            }
        }
        usort($this->teamInfo, function($a, $b){ return $b['rating'] - $a['rating']; });
    }
        
    public function actionInitialRating()
    {
        $r = Yii::app()->request->getParam('ratings', null);
        if($r) {
            foreach($r as $key => $value ) {
                $r = SwissRating::model()->findByPk($key);
                if(!$r) {
                    $r = new SwissRating();
                    $r->id = $key;
                }
                $r->rating = $value;
                $r->save();
            }
            return $this->refresh();
        }
        
        $this->teamInfo = Team::model()->getAllWithResults();
        $this->setInitialRating();
        $this->render('rating', ['teams' => $this->teamInfo]);
    }
}