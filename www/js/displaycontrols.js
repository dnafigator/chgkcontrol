$(document).ready(function(){
	
	$('.video').on('click', function(){
		$.ajax({
			url:'/display/video',
			data:{file:this.getAttribute('href')}
		});
		return false;
	});
	$('.image').on('click', function(){
		$.ajax({
			url:'/display/image',
			data:{file:this.getAttribute('href')}
		});
		return false;
	});
	
	$('.simplecountdown').on('click', function(){
		$.ajax({
			url:'/display/simplecountdown',
			data:{file:this.getAttribute('sec')}
		});
		return false;
	});

	$('.results').on('click', function(){
		$.ajax({
			url:'/display/results'
		});
		return false;
	});
});