$(document).ready(function(){

	$('#complete').on('click', function(){
		$.ajax({
				url:'/answers/commit', 
				data:
				{
					questionId:currentQuestion
				}
		}).done (function() {
			location.replace ( '/runtime/index' );
		});
	});
	$('#close').on('click', function(){
		location.replace ( '/runtime/index' );
	});

	$('.rightAnswer').on('click', function(){
		var data = $(this).attr('id').split ( ' ' );
		console.log ( data );
		$.ajax({
			url:'/answers/add',
			data:{
				teamId:data [ 1 ],
				questionId:data [ 0 ]
			}}).done (function(){
					location.replace ( location.href );
				})

	});
	$('.wrongAnswer').on('click', function(){
		var data = $(this).attr('id').split ( ' ' );
		console.log ( data );
		$.ajax({
			url:'/answers/delete',
			data:{
				teamId:data [ 1 ],
				questionId:data [ 0 ]
			}}).done (function(){
					location.replace ( location.href );
				})

	});
});