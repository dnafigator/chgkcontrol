$(document).ready(function(){
	
	$('#addAnswerCode').focus();

	$('#tourNum').on('change', function(){
		$.ajax({
			url:'/runtime/setQuestion',
			data:{questionNum: 1},
			success:function(){
				$('#commitForm').submit();
			}
		});
	});
	
	$('#showTour').on('change', function(){
		var tourNum = $(this).val();
//		$('#questionNum').val('1');
		$.ajax({
			url:'/runtime/setTour',
			data:{tourNum:tourNum}
		});
	});
	
	
	$('#questionNum').on('change', function(){
		var questionNum = $(this).val();
		$.ajax({
			url:'/runtime/setQuestion',
			data:{questionNum: questionNum}
		}).done (function(){

			var questionNum = getCurrentQuestion();
			var rightClass = '.question'+questionNum;
			$('.current-question').removeClass ( 'current-question' );
			$(rightClass).addClass ( 'current-question' );


		});
	});
		
	function isBarcode(code) {
		return /0000[0-9][0-9][0-9]00[0-9][0-9][0-9]/.test(code);
	}

	function isBarcodeTeamOnly(code) {
		return /2[0-9][0-9][0-9]00000000[0-9]/.test(code);
	}
	
	function checkControlSum(code) {
		var r = code.split("").reverse().join("");
		var sum = 0;
		for (var i=0; i<r.length; ++i) {
			sum += i % 2 === 0 ? parseInt(r[i], 10) : parseInt(r[i], 10)*3;
		}
		return sum % 10 === 0;
	}
	
	function parseBarcode(code) {
		
		if (!checkControlSum(code)) {
			return {error:true};
		}

		return {
			team : parseInt(code.substring(4, 7), 10),
			question : parseInt(code.substring(9, 11), 10)
		};
	}
	
	function parseBarcodeTeamOnly(code) {
		
		if (!checkControlSum(code)) {
			return {error:true};
		}

		var tourNum = getCurrentTour();
		var questionNum = getCurrentQuestion();
		var questionId = getQuestionId(questionNum, tourNum);
		return {
			team : parseInt(code.substring(2, 4), 10),
			question : parseInt(questionId, 10)
		};
	}
	
	
	function isHumanCode(code) {
		return /[0-9]+\s[0-9]+/.test(code);
	}
	
	function parseHumanCode(code) {
		var splited = code.split(' ');
		if (splited.length !== 2) {
			return {error:true};
		}
		return {
			team:parseInt(splited[1], 10),
			question:parseInt(splited[0], 10)
		};
	}

	function isSimpleHumanCode(code) {
		return /[0-9]+/.test(code);
	}
	
	function parseSimpleHumanCode(code) {
		var tourNum = getCurrentTour();
		var questionNum = getCurrentQuestion();
		var questionId = getQuestionId(questionNum, tourNum);

		return {
			team:parseInt(code, 10),
			question:parseInt(questionId, 10)
		};
	}
	
	function parseCode(code) {
		if (isBarcode(code)) {
			return parseBarcode(code);
		}
		if (isBarcodeTeamOnly(code)) {
			return parseBarcodeTeamOnly(code);
		}
		
		if (isHumanCode(code)) {
			return parseHumanCode(code);
		}
		if (isSimpleHumanCode(code)) {
			return parseSimpleHumanCode(code);
		}
		
		return false;
	}
	
	$('#answersForm').on('submit', function(){
		var $codeInput = $('#addAnswerCode');
		var code = $codeInput.val();
		$codeInput.val('');
		
//		console.log(code);
//		console.log('is barcode: '+isBarcode(code));
		
		var data = parseCode(code);
		if (false === data) {
			alert('Неверный формат кода ответа');
		}
		else if (data.error) {
			alert('Ошибка чтения кода');
		}
		else {
			var questionNum = getQuestionNum(data.question);
			var tourNum = getTourNum(data.question);
			var curQuestion = getQuestionId(getCurrentQuestion(),getCurrentTour())
			if ( data.question == curQuestion || confirm('Номер бланка не соответстует текущему вопросу ('+data.question+' != '+curQuestion+'). Добавить ответ?')) {
				$.ajax({
					url: '/answers/add',
					data: {
						teamId: data.team,
						questionId: data.question
					},
					success: function () {
						if (getCurrentTour() == tourNum) {
							var $cell = $('#answer' + data.team + '_' + questionNum);
							if ($.trim($cell.text()) != '+') {
								$cell.addClass('no-commited');
								var q = 1+(+$('.answered>.answeredQ'+questionNum).text());
								$('.answered>.answeredQ'+questionNum).text(+q);
								var p = $('.answeredPercent').attr('data-count');
								$('.answeredPercent>.answeredPercentQ'+questionNum).text(Math.floor(10000*q/p)/100);
							}
							$cell.html('+');
						}
					}
				});
			}
		}
		
		return false;
	});

	$('#aAnswersInit').on('click', function(){
		$('#aAnswerText').val('');
		$('#aAnswerCode').val('');
                $("#aAnswerCode").removeAttr('disabled');
		$('#aAnswerFormDialog').one('shown.bs.modal', 0, function (){
			$('#aAnswerCode').focus();
		}).modal('show');
		return false;
	});
	$('#aAnswerCode').keypress(function(event) {
		if ( event.keyCode == 13 )
		{
			$('#aAnswerText').focus();
			return false;
		}
		else
		{
			return true;
		}
	});

//TODO: убрать костыль!!!
	$('#aAnswerText').keypress(function(event) {
		if ( event.keyCode == 13 )
		{
			$('#aAnswerForm').submit ();
		}
		return true;
	});
//TODO end

	$('#aAnswerSubmit').on('click', function(){
		$('#aAnswerForm').submit ();
	});

	$('#aAnswerCancel').on('click', function(){
		$('#aAnswerText').val('');
		$('#aAnswerCode').val('');
	});

	$('#aAnswerForm').on('submit', function(){
		var $codeInput = $('#aAnswerCode');
		var code = $codeInput.val();
		
		console.log('!'+code);
//		console.log('is barcode: '+isBarcode(code));
		
		var data = parseCode(code);
		if (false === data) {
			alert('Неверный формат кода ответа');
		}
		else if (data.error) {
			alert('Ошибка чтения кода');
		}
		else {
			var questionNum = getQuestionNum(data.question);
			var tourNum = getTourNum(data.question);
			
			$.ajax({
				url:'/answers/addArguable',
				data:{
					teamId:data.team,
					questionId:data.question,
					text:$('#aAnswerText').val()
				},
				success: function() {
					$('#aAnswerText').val('');
					$('#aAnswerCode').val('');
					if (getCurrentTour() == tourNum) {
						var $cell = $('#answer'+data.team+'_'+questionNum);
						$cell.addClass('no-commited');
                                                if ( -1 == $cell.html ().indexOf ( '+' ) )
                                                {
                                                    $cell.html('?');
                                                }
					}					
				}
			});
		}
		
		return false;
	});
	

	$('#showphotos').on('click', function(){
		var tourNum = getCurrentTour();
		var questionNum = getCurrentQuestion();
		var questionId = getQuestionId(questionNum, tourNum);
		location.replace ( '/photoAnswer/index/question/'+questionId );
	});

	$('#commitAnswers').on('click', function(){
		var tourNum = getCurrentTour();
		var questionNum = getCurrentQuestion();
		var questionId = getQuestionId(questionNum, tourNum);
		//console.log("Commiting answers");

		$.ajax({
				url:'/answers/commit', 
				data:
				{
					questionId:questionId
				}
		}).done (function(data) {
				console.log(data);
				var questionNum = getCurrentQuestion();
				/*var required_classes = '.no-commited.question' + questionNum;
				console.log( required_classes );
				$(required_classes).removeClass ( 'no-commited' );*/

				if (questionNum != questionInTourCount) {
					questionNum++;
					$.ajax({
							url:'/runtime/setQuestion',
							data:{questionNum: questionNum}
						}).done(function(){
							//console.log("ok2!");
							location.replace(location.href);
						})
				}
				else {
					location.replace(location.href);
				}
			});
	});
	
	$('#answersCancelForm').on('submit', function(){
		var $codeInput = $('#deleteAnswerCode');
		var code = $codeInput.val();
		$codeInput.val('');		
		
		var data = parseCode(code);
		if (false === data) {
			alert('Неверный формат кода ответа');
		}
		else if (data.error) {
			alert('Ошибка чтения кода');
		}
		else {
			
			if (! confirm('Вы действительно хотите удалить ответ команды '+data.team+' на вопрос '+data.question+'?')) {
				return false;
			}
			
			var questionNum = getQuestionNum(data.question);
			var tourNum = getTourNum(data.question);
			
			$.ajax({
				url:'/answers/delete',
				data:{
					teamId:data.team,
					questionId:data.question
				},
				success: function() {
					if (getCurrentTour() == tourNum) {
						var $cell = $('#answer'+data.team+'_'+questionNum);
						$cell.html('-');
						$cell.removeClass('no-commited');
						location.replace(location.href);
					}	
				}
			});
		}		
		
		return false;
	});
	
	function getCurrentTour() {
		return $('#tourNum').val();
	}
	
	function getCurrentQuestion() {
		return $('#questionNum').val();
	}
	
	function getQuestionId(questionNum, tourNum) {
		return (parseInt(tourNum, 10)-1)*questionInTourCount + parseInt(questionNum, 10);
	}
	
	function getQuestionNum(questionId) {
		var questionNum = parseInt(questionId, 10) % questionInTourCount;
		if (questionNum === 0) {
			questionNum = questionInTourCount;
		}
		return questionNum;
	}
	
	function getTourNum(questionId) {
		return Math.floor((questionId-1) / questionInTourCount) + 1;
	}
	
	function getShowTour() {
		var showTour = $('#tourNum').val();
		if (showTour == 0) {
			showTour = getCurrentTour();
		}
		return showTour;
	}

	var questionNum = getCurrentQuestion();
	var rightClass = '.question'+questionNum;
	$(rightClass).addClass ( 'current-question' );


});