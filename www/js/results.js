var Results = {
	data:null,
	time:0,
	currentPage:0,
	pagesCount:0,
	PAGE_SIZE:0,
	lock: false,
	cmd: null,
	parm: null,
	countdownSec: 0,
	countdownTen: false,
	countdownTimer: null	
};

Results.longPolling = function () {
	time = Results.time || -1;
	$.ajax(
			{url: '/results/longdata', data: {time:time}}
		).done( function (data){
			console.log ( data );
			Results.longPolling ();
			});
			;
};

Results.loadData = function(callback, time) {
	time = time || -1;
	$.ajax({
		url: '/results/data',
		data: {
			time:time
		},
		success: function(resp){
			var data = $.parseJSON(resp);
			Results.cmd = data.cmd;
			Results.parm = data.parm;
			if ('update' == Results.cmd) {
				Results.data = data.data;
				Results.setTour(data.tourNum);
				Results.pagesCount = Math.floor(data.data.length / Results.PAGE_SIZE) + 1;
				Results.time = data.time;
			}
			if ( Results.parm )
			{
				callback.call();
			}
		}
	});
};

Results.putDataRow = function(num, rowNum) {
	if (Results.data == null) {
		return;
	}
	
	if (num >= Results.data.length) {
		return;
	}
	
	var $row = $('#row'+rowNum);
	
	var name = Results.data[num].name;
	if (Results.data[num].city) {
		name += ' <span class="results-team-city">('+Results.data[num].city+')</span>';
	}
	
	$('.results-team', $row).html(name);
        if ( Results.data[num].highlight > 0 )
        {
            $('.results-team', $row).addClass('results-highlight');
        }
        else
        {
            $('.results-team', $row).removeClass('results-highlight');
        }
	$('.results-place', $row).html(Results.data[num].place);
	$('.results-points', $row).html(Results.data[num].points);
	$('.results-tour-points', $row).html(Results.data[num].tour_points);
	$('.results-answer > span', $row).each(function(i){
		
		var a = Results.data[num].tour_answers[(i+1)+""];
		if (typeof a != 'undefined') {
			$(this).html('<img style="display:block; margin: 0 auto;"  src="'+imgPath+'correct_answer.png?4"/>');
			//$(this).addClass('ok');
			$(this).fadeIn(1);
		}
		else {
			$(this).html('<img style="display:block; margin: 0 auto;"  src="'+imgPath+'incorrect_answer.png?4"/>');
			//$(this).html('');
			//$(this).removeClass('ok');
		}
	});
	
};

Results.rotate = function(noChangePage) {
	
	if (typeof noChangePage == 'undefined') {
		noChangePage = false;
	}
	
	if (noChangePage) {
		if (Results.currentPage >= Results.data.length) {
			Results.currentPage = 0;
		}
	}
	else {
		if (Results.pagesCount == 1) {
			return;
		}		
		Results.setNextPageNum();
	}
	
	var gen = function(i){
		return function(){
			var $row = $('#row'+i+' span');
			$row.fadeOut(400);
			setTimeout(function(){
				setTimeout(function(){
					var num = Results.currentPage*Results.PAGE_SIZE + i;
					if (num < Results.data.length) {
						Results.putDataRow(num, i);
						$row.fadeIn();
					}
				}, 200);
			}, 400);
		};
	};
	
	for (var i=0; i<Results.PAGE_SIZE; ++i) {
		setTimeout(gen(i), 200+i*100);
	}
};

Results.setTour = function(tourNum) {
	if ( tourNumViaBG )
	{
		var offset = (8 - tourNum) * 53;
		$('#results-tour-num').css('background-position', '0px '+offset+'px');
	}
	else
	{
		$('#results-tour-num').html ( tourNum + "&nbsp;тур" );
	}
	
	$('td.results-table-one-answer.head').each(function(i){
		$(this).html((tourNum-1)*questionInTourCount + i + 1);
	});
};

Results.setNextPageNum = function() {
	Results.currentPage++;
	if (Results.currentPage >= Results.pagesCount) {
		Results.currentPage = 0;
	}
};

Results.startRotateLoop = function() {
	Results.rotateIntervalId = setInterval(function(){
		Results.rotate();
	}, 10000);	
};

Results.stopRotateLoop = function() {
	clearInterval(Results.rotateIntervalId);
};


Results.showVideo = function ()
{
	Results.hideSimpleCountdown();
	$('.results-header,.results-table').hide();
	$('#imageplayer').hide();
	$('#videoplayer').get(0).setAttribute ( 'src', Results.parm );	
	$('#videoplayer').show();
};

Results.showImage = function ()
{
	Results.hideSimpleCountdown();
	$('.results-header,.results-table').hide();
	$('#videoplayer').get(0).pause();
	$('#videoplayer').get(0).src = '';
	$('#videoplayer').get(0).removeAttribute ( 'src' );
	$('#videoplayer').hide();
	
	$('#imageplayer').get(0).setAttribute ( 'src', Results.parm );	
	$('#imageplayer').show();
};

Results.showResults = function ()
{
	Results.hideSimpleCountdown();
	$('#videoplayer').get(0).pause();
	$('#videoplayer').get(0).src = '';
	$('#videoplayer').get(0).removeAttribute ( 'src' );
	$('#videoplayer').hide();
	
	$('.results-header,.results-table').show();
	$('#imageplayer').hide();
};

function playSound ( snd )
{
	$('#audioplayer').get(0).setAttribute ( 'src', imgPath+snd );	
}

Results.startSimpleCountdown = function()
{
	if (Results.countdownTimer)
	{
		clearInterval ( Results.countdownTimer );
		Results.countdownTimer = null;
	}
	Results.countdownTen = Results.parm.indexOf ( '+' ) > -1;
	Results.countdownSec  = parseInt( Results.parm.replace ( '+', '' ) );
	$('#countdown').removeClass ( 'red' );

	//beep start
	playSound ( 'start.ogg' );
	
	$('#countdown').html ( Results.countdownSec );
	Results.countdownTimer = setInterval ( function(){
		Results.countdownSec --;
		var sec = Results.countdownSec;
		if ( 10 === Results.countdownSec )
		{
			playSound ( 'ten.ogg' );
		}
		else if ( 0 === Results.countdownSec )
		{
			playSound ( 'zero.ogg' );
		}
		else if ( Results.countdownSec < 0 )
		{
			$('#countdown').addClass ( 'red' );
			if ( Results.countdownTen )
			{
				sec = 10 + Results.countdownSec;
				if ( Results.countdownSec < -10 )
				{
					sec = 0;
					if ( Results.countdownSec < -11 )
					{
						clearInterval ( Results.countdownTimer );
						Results.countdownTimer = null;
						Results.showResults();
					}
					else
					{
						playSound ( 'end.ogg' );
					}
				}
				else
				{
					playSound ( 'tick.ogg' );					
				}
			}
			else
			{
				clearInterval ( Results.countdownTimer );
				sec = 0;
				Results.countdownTimer = null;
				Results.showResults();
			}
		}
		$('#countdown').html ( sec );
	}, 1000);
};

Results.hideSimpleCountdown = function()
{
	if (Results.countdownTimer)
	{
		clearInterval ( Results.countdownTimer );
		Results.countdownTimer = null;
	}
	$('.cdContainer').hide();
}

Results.showSimpleCountdown = function ()
{
	$('#videoplayer').get(0).pause();
	$('#videoplayer').get(0).src = '';
	$('#videoplayer').get(0).removeAttribute ( 'src' );
	$('#videoplayer').hide();
	$('.results-header,.results-table').hide();
	$('#imageplayer').hide();
	$('.cdContainer').show();
	Results.startSimpleCountdown();
}

$(document).ready(function(){
	Results.PAGE_SIZE = page_size;

	Results.loadData(
		function(){
			for (var i=0; i<Results.data.length; ++i) {
				Results.putDataRow(i, i);
			}
		},
		-1);
	
	Results.startRotateLoop();
	
	setInterval(function(){
		Results.loadData(
			function(){
				console.log ( Results );
				switch ( Results.cmd )
				{
					case 'video':
							Results.showVideo();
						break;
					case 'image':
							Results.showImage();
						break;
					case 'results':
							Results.showResults();
						break;
					case 'simplecountdown':
							Results.showSimpleCountdown();
						break;
					case 'update':
							Results.stopRotateLoop();
							Results.rotate(true);
							Results.startRotateLoop();
						break;
				}
			},
			Results.time);
	}, 2000);
	
//	Results.longPolling ();
	 $('#videoplayer').on ( 'ended error', function (){
		 Results.showResults();
	 });
	$('#videoplayer').on ( 'loadedmetadata canplay loadeddata', function (e){
		$(this).get(0).seekable.start(0);
		$(this).get(0).play();
	});
	$('#audioplayer').on ( 'loadedmetadata canplay loadeddata', function (e){
		$(this).get(0).seekable.start(0);
		$(this).get(0).play();
	});
	 
});
