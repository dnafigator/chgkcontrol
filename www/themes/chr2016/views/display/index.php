Чернозёмфест 2015
<button class="btn results">Таблица результатов</button>
<label class="control-label">Отсчёты</label>
<button href="60+10.mp4" sec="60+" class="btn video">Вопрос (60+10)</button><br/>
<label class="control-label">Дуплет</label>
<button href="30.mp4" sec="30" class="btn video">Первый вопрос (30)</button><br/>
<button href="30+10.mp4" sec="30+" class="btn video">Второй вопрос (30+10)</button>
<label class="control-label">Блиц</label>
<button href="20.mp4" sec="20" class="btn video">Первый вопрос (20)</button><br/>
<button href="20.mp4" sec="20" class="btn video">Второй вопрос (20)</button><br/>
<button href="20+10.mp4" sec="20+" class="btn video">Последний вопрос (20+10)</button>
<label class="control-label">Управление</label>
<!--button href="intro.mp4" class="btn video">Интро</button><br/-->
<button href="logo.png" class="btn image">Лого фестиваля</button><br/>

<label class="control-label">Лого спонсоров</label>
<button href="logo_vsu.png" class="btn image">ВГУ</button><br/>
<button href="logo_osovsu.png" class="btn image">ОСО ВГУ</button><br/>
<button href="logo_ehall.png" class="btn image">Ивент-холл</button><br/>
<button href="logo_omc.png" class="btn image">ОМЦ</button><br/>
<button href="logo_hamin.png" class="btn image">ГК Хамина</button><br/>
<button href="logo_dataart.png" class="btn image">DataArt</button><br/>
<button href="logo_inti.png" class="btn image">ООО &laquo;ИНТИ&raquo;</button><br/>
<button href="logo_holidayinn.png" class="btn image">Холидей Инн</button><br/>
<button href="logo_degas.png" class="btn image">Degas</button><br/>
<button href="logo_prosvdigital.png" class="btn image">Просвещение Диджитал</button><br/>

<label class="control-label">Раздача</label>
<button href="dist_05.png" class="btn image">Вопрос 5 (тур 1 вопрос 5)</button><br/>
<button href="dist_10.png" class="btn image">Вопрос 10 (тур 1 вопрос 10)</button><br/>
<button href="dist_20.png" class="btn image">Вопрос 20 (тур 2 вопрос 5)</button><br/>
<button href="dist_36.png" class="btn image">Вопрос 36 (тур 3 вопрос 6)</button><br/>
<button href="dist_40.png" class="btn image">Вопрос 40 (тур 3 вопрос 10)</button><br/>
<button href="dist_41.png" class="btn image">Вопрос 41 (тур 3 вопрос 11)</button><br/>
<button href="dist_51.png" class="btn image">Вопрос 51 (тур 4 вопрос 6)</button><br/>
<button href="dist_63.png" class="btn image">Вопрос 63 (тур 5 вопрос 3)</button><br/>
<button href="dist_70.png" class="btn image">Вопрос 70 (тур 5 вопрос 10)</button><br/>
<button href="dist_74.png" class="btn image">Вопрос 74 (тур 5 вопрос 14)</button><br/>
<button href="dist_77.png" class="btn image">Вопрос 77 (тур 6 вопрос 2)</button><br/>
<button href="dist_90.png" class="btn image">Вопрос 90 (тур 6 вопрос 15)</button><br/>
