<!DOCTYPE html><?php $page_size = 15;?>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript">
            	    var tourCount = <?php echo $this->tourCount; ?>;
            	    var questionInTourCount = <?php echo $this->questionInTourCount; ?>;
					var imgPath = "<?php echo $this->imgPath; ?>";
					var tourNumViaBG = false;
					var page_size = <?php echo $page_size;?>;
                </script>
		<?php  
			Yii::app()->clientScript->registerCoreScript('jquery')
		?>
	</head>
	<body class="results-back">
		<div class="results-header">
			<div class="results-logo"></div>
			<div class="results-tour-num" id="results-tour-num"></div>
		</div>
		
		<table class="results-table" cellspacing="0">
			<thead>
			<tr>
				<td rowspan="2">Место</td>
				<td rowspan="2">Название команды</td>
				<td colspan="<?php echo $this->questionInTourCount?>" class="results-table-answers">Результаты текущего тура</td>
				<td rowspan="2">Баллы<br/>за<br/>тур</td>
				<td rowspan="2">Общий<br/>результат</td>
			</tr>
			<tr>
				<?php for ($i=1; $i <= $this->questionInTourCount; $i++ ): ?>
					<td class="results-table-one-answer head"><?php echo $i; ?></td>
				<?php endfor; ?>
			</tr>
			</thead>
			
			<tbody>
				<?php for($i=0; $i<$page_size; $i++): ?>
					<tr id="row<?php echo $i?>">
					<td><span class="results-place"></span></td>
					<td class="results-team-cell"><span class="results-team"></span></td>
					<?php for ( $j = 0; $j < $this->questionInTourCount; $j ++ ):?>
					<td class="results-table-one-answer results-answer"><span></span></td>
					<?php endfor; ?>

					<td><span class="results-tour-points"></span></td>
					<td><span class="results-points"></span></td>						
					</tr>
				<?php endfor; ?>
		
				
			</tbody>
		
		</table>
		
	</body>
</html>
